using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;

public class Main : SingletonMonoBehaviourClass<Main>
{
    protected override void onAwake()
    {
        ModuleManager.instance.InitAllModules();
    }

    void Update()
    {
        ModuleManager.instance.Update();
    }

    void FixedUpdate()
    {
        ModuleManager.instance.FixedUpdate();
    }
}
