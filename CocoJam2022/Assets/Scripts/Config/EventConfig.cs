using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public enum eEventType
    {
        Cook,//做饭
        Work,//工作
        Emergency,//突发事件
        Sleep,//睡觉
        Buy,//购买
        SettleRes,//整理冰箱
        Play,
        Rest,
        Quarantine,//隔离
        Free,//解封
    }
    public enum eInterruptType
    {
        Any,
        Specify,
        Expect,
        None,
    }
    [CreateAssetMenu(fileName = "EventConfig", menuName = "CreateConfig/EventConfig")]
    [Serializable]
    public class EventConfig : ScriptableObject
    {
        public EventItemConfig[] events;

        public EventItemConfig GetConfigByType(eEventType type)
        {
            foreach (var config in events)
            {
                if (config.eventType == type)
                {
                    return config;
                }
            }
            return null;
        }

        public EventItemConfig GetConfigByName(string name)
        {
            foreach (var config in events)
            {
                if (config.eventName == name)
                {
                    return config;
                }
            }
            return null;
        }
        private List<EventItemConfig> tempConfigs = new List<EventItemConfig>();
        public EventItemConfig GetRandomEmergency(EventItemConfig currentConfig)
        {

            if (!currentConfig.CanBeInterruptedByType(eEventType.Emergency)) return null;
            tempConfigs.Clear();
            foreach (var config in events)
            {
                if (config.eventType == eEventType.Emergency && config.CanInterruptOtherType(currentConfig.eventType) && (UnityEngine.Random.value < config.emergencyProbability))
                {
                    tempConfigs.Add(config);
                }
            }

            if (tempConfigs.Count == 0) return null;
            if (tempConfigs.Count == 1) return tempConfigs[0];
            else return tempConfigs[UnityEngine.Random.Range(0, tempConfigs.Count)];
        }
    }

    [Serializable]
    public class EventItemConfig : IEffect
    {
        [Header("基础信息")]
        public string eventName;
        public string id;
        public eEventType eventType;
        public int costTime; //分钟
        [Tooltip("事件描述")]
        public string desc;
        [Header("事件效果")]
        [Tooltip("事件结束后增加的数值")]
        public float changeInfection;
        public int changeEMO;
        public int changeHealth;
        public int changeMoney;
        [Header("打断设定")]
        public eInterruptType beInterruptType;
        public List<eEventType> beInterruptEventType;
        public eInterruptType interruptOtherType;
        public List<eEventType> interruptOtherEventType;
        [Header("随机事件设定")]
        [Range(0, 1)]
        [Tooltip("触发随机事件的概率")]
        public float emergencyProbability = 0;
        [Tooltip("随机事件增加的物资")]
        public FoodMaterial[] addItemList;
        [Header("限制参数")]
        [Tooltip("每天的可执行次数  -1为无限次")]
        public int oneDayTimes = -1;//-1无限制
        public int limitMinHour = 0;
        public int limitMaxHour = 24;

        float IEffect.changeInfection => changeInfection;

        int IEffect.changeEMO => changeEMO;

        int IEffect.changeHealth => changeHealth;

        int IEffect.changeMoney => changeMoney;

        public bool CanBeInterruptedByType(eEventType type)
        {
            if (beInterruptType == eInterruptType.None) return false;
            if (beInterruptType == eInterruptType.Specify && !beInterruptEventType.Contains(type)) return false;
            if (beInterruptType == eInterruptType.Expect && beInterruptEventType.Contains(type)) return false;
            return true;
        }

        public bool CanInterruptOtherType(eEventType type)
        {
            if (interruptOtherType == eInterruptType.None) return false;
            if (interruptOtherType == eInterruptType.Specify && !interruptOtherEventType.Contains(type)) return false;
            if (interruptOtherType == eInterruptType.Expect && interruptOtherEventType.Contains(type)) return false;
            return true;
        }
    }
}

