using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public class ConfigManager : SingletonMonoBehaviourClass<ConfigManager>
    {
        public GameConfig gameConfig;
        public EventConfig eventConfig;
        public PlayerConfig playerConfig;

        public ItemConfig itemConfig;
        public RefrigeratorConfig refrigeratorConfig;
        public CookConfig cookConfig;
        public BuyConfig buyConfig;
    }
}
