using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "RefrigeratorConfig", menuName = "CreateConfig/RefrigeratorConfig")]
    [Serializable]
    public class RefrigeratorConfig : ScriptableObject
    {
        [Header("上层配置")]
        public Vector2Int upSize;
        public RefrigeratorObstacleConfig[] upObstacleConfigs;
        [Header("下层配置")]
        public Vector2Int downSize;
        public RefrigeratorObstacleConfig[] downObstacleConfigs;

        [Header("初始物资")]
        public FoodMaterial[] initRes;
    }

    [Serializable]
    public class RefrigeratorObstacleConfig
    {
        public Vector2Int obstaclePos;
        public int hideLevel;
    }
}

