namespace Config
{
    public interface IEffect
    {
        float changeInfection { get; }
        int changeEMO { get; }
        int changeHealth { get; }
        int changeMoney { get; }
    }
}