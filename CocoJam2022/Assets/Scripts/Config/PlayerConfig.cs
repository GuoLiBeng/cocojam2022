using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public enum eAbilityType
    {
        Settle,
        Cook,
    }
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "CreateConfig/PlayerConfig")]
    [Serializable]
    public class PlayerConfig : ScriptableObject
    {
        [Header("初始参数")]
        [Range(0, 1)]
        public float infection;
        public int emo;
        public int health;
        public int money;

        [Header("范围限制")]
        public int maxEmo;
        public int maxHealth;
        [Header("能力  每级所需的经验")]
        [Tooltip("做饭能力")]
        public int[] abilityCookExp;
        [Tooltip("整理能力")]
        public int[] abilitySettleExp;

        public int GetUpgradeNeedExp(eAbilityType type, int level)
        {
            int[] list = null;
            switch (type)
            {
                case eAbilityType.Cook:
                    list = abilityCookExp;
                    break;
                case eAbilityType.Settle:
                    list = abilitySettleExp;
                    break;
            }
            return list.Length > level ? list[level] : list[list.Length - 1];
        }
        public string GetAbilityStringByType(eAbilityType type)
        {
            switch (type)
            {
                case eAbilityType.Cook:
                    return "做饭能力";
                case eAbilityType.Settle:
                    return "整理能力";
            }
            return "error";
        }
    }
}

