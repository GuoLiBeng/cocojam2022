using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "CookConfig", menuName = "CreateConfig/CookConfig")]
    [Serializable]
    public class CookConfig : ScriptableObject
    {
        public MenuConfig[] menuConfigs;
    }

    [Serializable]
    public class MenuConfig : IEffect
    {
        public string name;
        public string desc;
        public int costTime;
        [Header("原料")]
        public FoodMaterial[] materials;
        [Header("食用效果")]
        public float changeInfection;
        public int changeEMO;
        public int changeHealth;
        public int changeMoney;

        float IEffect.changeInfection => changeInfection;

        int IEffect.changeEMO => changeEMO;

        int IEffect.changeHealth => changeHealth;

        int IEffect.changeMoney => changeMoney;
    }

    [Serializable]
    public class FoodMaterial
    {
        public string itemName;
        public int amount;
    }
}

