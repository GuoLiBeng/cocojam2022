using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "BuyConfig", menuName = "CreateConfig/BuyConfig")]
    [Serializable]
    public class BuyConfig : ScriptableObject
    {
        [Header("抢购设置")]
        public float minBuyTime;
        public float maxBuyTime;
        public float minLoadingTime;
        public float maxLoadingTime;
        [Range(0, 1)]
        public float successRate;

        [Header("抢购物资配置")]
        public int minItemTypeAmount;
        public int maxItemTypeAmount;
        public int minItemAmount;
        public int maxItemAmount;
    }
}

