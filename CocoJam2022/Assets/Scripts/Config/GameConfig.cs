using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "CreateConfig/GameConfig")]
    [Serializable]
    public class GameConfig : ScriptableObject
    {
        [Header("初始解封概率")]
        public float freeChance;
        [Header("每天增加的解封概率")]
        public float addFreeChance;
        [Header("封控天数")]
        public int[] quarantineDays;


    }
}

