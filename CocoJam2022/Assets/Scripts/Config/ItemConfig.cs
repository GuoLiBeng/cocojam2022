using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "ItemConfig", menuName = "CreateConfig/ItemConfig")]
    [Serializable]
    public class ItemConfig : ScriptableObject
    {
        public ItemConfigItem[] items;
        [NonSerialized]
        private List<ItemConfigItem> tempList;
        public ItemConfigItem GetItemConfigByName(string name)
        {
            tempList ??= new List<ItemConfigItem>();
            tempList.Clear();
            foreach (var item in items)
            {
                if (item.itemName == name)
                {
                    tempList.Add(item);
                }
            }
            if (tempList.Count > 0)
            {
                return tempList[UnityEngine.Random.Range(0, tempList.Count)];
            }
            else
            {
                return null;
            }
        }
    }

    [Serializable]
    public class ItemConfigItem : IEffect
    {
        [Header("基础信息")]
        public string itemName;
        public string id;
        public Sprite icon;
        public int Life;
        public Vector2Int[] shape;
        public float imgScale = 1.0f;
        public string desc;
        public bool canDirectlyEat = false;
        public bool isLiquid = false;

        [Header("食用效果")]
        public float changeInfection;
        public int changeHealth;
        public int changeEMO;
        public int changeMoney;

        float IEffect.changeInfection => changeInfection;

        int IEffect.changeEMO => changeEMO;

        int IEffect.changeHealth => changeHealth;

        int IEffect.changeMoney => changeMoney;
    }
}

