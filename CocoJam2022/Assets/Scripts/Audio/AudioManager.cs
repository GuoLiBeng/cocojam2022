using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;

public class AudioManager : SingletonMonoBehaviourClass<AudioManager>
{
    public AudioClip[] clips;
    public AudioClip[] dayRadioClips;
    public AudioClip[] nightClips;
    private bool m_radioEnable = false;
    public bool radioEnable
    {
        get => m_radioEnable;
        set
        {
            m_radioEnable = value;
            if (value)
            {
                AudioManager.instance.PlayEffect("UI_RadioPlay");
                StopBGM(true);
            }
            else
            {
                AudioManager.instance.PlayEffect("UI_RadioStop");
                StopBGM(true);
            }
        }
    }
    public AudioPlayer BGM;
    //    private Stack<AudioClip> BGMStack;
    public AudioPlayer AGM;
    public MultiItemManager EffectList;
    public MultiItemManager SingletonEffectList;
    private Dictionary<string, AudioClip> m_audioDict;
    private Dictionary<string, AudioClip> audioDict
    {
        get
        {
            if (m_audioDict == null)
            {
                m_audioDict = new Dictionary<string, AudioClip>();
                foreach (var clip in clips)
                {
                    audioDict.Add(clip.name, clip);
                }
            }
            return m_audioDict;
        }
    }

    private Dictionary<string, AudioPlayer> singletonEffectDict;
    private AudioClip baseBGM
    {
        get
        {
            if (m_radioEnable)
            {
                BGM.maxVolume = 0.6f;
                (int hour, int minute) = StaticUtils.MinuteToHour(GameModel.instance.currentTime);
                if (hour >= 6 && hour < 20)
                {
                    return dayRadioClips[Random.Range(0, dayRadioClips.Length)];
                }
                else
                {
                    return nightClips[Random.Range(0, nightClips.Length)];
                }
            }
            else
            {
                BGM.maxVolume = 1.0f;
                return audioDict["bgm_init_loop"];
            }
        }
    }


    protected override void onAwake()
    {
        EffectList.SetSize(1);
        singletonEffectDict = new Dictionary<string, AudioPlayer>();
        //BGMStack = new Stack<AudioClip>();
        //BGMStack.Push(audioDict["bgm_init_loop"]);
    }

    public void PlayEffect(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            for (int i = 0; i < EffectList.Size; i++)
            {
                var source = EffectList.GetItem<AudioSource>(i);
                if (!source.isPlaying)
                {
                    source.clip = clip;
                    source.Play();
                    return;
                }
            }

            var newSource = EffectList.Expand().GetComponent<AudioSource>();
            newSource.clip = clip;
            newSource.Play();
        }
    }
    private bool isPlayingBGM;

    public void PlayBGM(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            isPlayingBGM = true;
            BGM.Play(clip, true);
            //BGMStack.Push(clip);
        }
    }

    // public void ChangeBGM(string name)
    // {
    //     if (audioDict.TryGetValue(name, out AudioClip clip))
    //     {
    //         BGM.Change(clip);
    //     }
    // }

    public void StopBGM(bool forceChange = false)
    {
        // BGMStack.Pop();
        // if (BGMStack.Count == 0)
        // {
        //     BGMStack.Push(audioDict["bgm_init_loop"]);
        // }
        if (forceChange || isPlayingBGM)
        {
            isPlayingBGM = false;
            BGM.Play(baseBGM, true);
        }
    }

    public void PlayAGM(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            AGM.Play(clip, true);
        }
    }

    public void StopAGM()
    {
        AGM.Stop();
    }

    public void PlaySingletonEffect(string name, bool loop = false, float fadeTime = 0.1f)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            if (!singletonEffectDict.ContainsKey(name))
            {
                singletonEffectDict.Add(name, SingletonEffectList.Expand().GetComponent<AudioPlayer>());
            }
            if (!singletonEffectDict[name].isPlaying)
            {
                singletonEffectDict[name].Play(clip, loop, fadeTime);
            }
        }
    }

    public void StopSingletonEffect(string name, float fadeTime = 0.2f)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            if (singletonEffectDict.ContainsKey(name))
            {
                if (singletonEffectDict[name].isPlaying)
                {
                    singletonEffectDict[name].Stop(fadeTime);
                }
            }
        }
    }

    public void PlayRewardSound()
    {
        PlayEffect("Reward");
    }

    void Update()
    {

    }
}