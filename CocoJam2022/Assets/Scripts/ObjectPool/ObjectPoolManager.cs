using UnityEngine;

namespace ObjectPool
{
    public class ObjectPoolManager : SingletonMonoBehaviourClass<ObjectPoolManager>
    {
        public Transform poolRoot { get => transform; }
        public ObjectPool<GameObject> UIPool = new ObjectPool<GameObject>(
            (object name) =>
            {
                return Resources.Load<GameObject>($"Prefabs/UI/{(string)name}");
            },
            (GameObject prefab) =>
            {
                var gameObject = GameObject.Instantiate<GameObject>(prefab);
                gameObject.SetActive(false);
                return gameObject;
            },
            (GameObject obj) =>
            {
                obj.transform.SetParent(ObjectPoolManager.instance.poolRoot);
                obj.SetActive(false);
            }
            );

        public ObjectPool<GameObject> ObjectPool = new ObjectPool<GameObject>(
            (object name) =>
            {
                return Resources.Load<GameObject>($"Prefabs/Object/{(string)name}");
            },
            (GameObject prefab) =>
            {
                var gameObject = GameObject.Instantiate<GameObject>(prefab);
                gameObject.SetActive(false);
                return gameObject;
            },
            (GameObject obj) =>
            {
                obj.transform.SetParent(ObjectPoolManager.instance.poolRoot);
                obj.SetActive(false);
            }
        );

        public ObjectPool<Material> MaterialPool = new ObjectPool<Material>(
            (object name) =>
            {
                return Resources.Load<Material>($"Materials/{(string)name}");
            },
            (Material prefab) =>
            {
                var mat = GameObject.Instantiate<Material>(prefab);
                return mat;
            }
        );
    }
}