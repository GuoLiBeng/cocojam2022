using UnityEngine;

namespace Cameras
{
    public class SceneCamera : SingletonMonoBehaviourClass<SceneCamera>
    {
        public Camera cameraObject;
        protected override void onAwake()
        {
            cameraObject = transform.GetComponent<Camera>();
        }
    }
}