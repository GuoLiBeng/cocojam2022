using UnityEngine;

namespace Cameras
{
    public class UICamera : SingletonMonoBehaviourClass<UICamera>
    {
        public Camera cameraObject;
        protected override void onAwake()
        {
            cameraObject = transform.GetComponent<Camera>();
        }
    }
}