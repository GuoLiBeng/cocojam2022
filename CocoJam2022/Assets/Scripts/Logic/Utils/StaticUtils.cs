using UnityEngine;
using Logic;
public static class StaticUtils
{
    public static string MinuteToString(int minute)
    {
        (int h, int m) = MinuteToHour(minute);
        return string.Format("{0:D2}:{1:D2}", h, m);
    }

    public static string MinuteToStringChinese(int minute)
    {
        if (minute < 0) return "永久";
        int day = minute / GameModel.AllDayTime;
        minute = minute % GameModel.AllDayTime;
        int h = minute / 60;
        int m = minute % 60;
        if (day > 0)
        {
            if (h > 0)
            {
                return $"{day}天{h}时";
            }
            else
            {
                return $"{day}天";
            }

        }
        else if (h > 0)
        {
            if (m > 0)
            {
                return $"{h}时{m}分";
            }
            else
            {
                return $"{h}时";
            }
        }
        else
        {
            return $"{m}分";
        }
    }

    public static (int hour, int minute) MinuteToHour(int minute)
    {
        int h = minute / 60;
        int m = minute % 60;
        return (h, m);
    }

    public enum ROTATE_DIRECTION
    {
        Left, //逆时针
        Right, //顺时针
        Horizon, //水平翻转
        Vertical, //竖直翻转
    }
    public static Vector2Int RotatePos(Vector2Int pos, Vector2Int center, ROTATE_DIRECTION dir)
    {
        if (dir == ROTATE_DIRECTION.Left)
        {
            return new Vector2Int(center.y - pos.y + center.x, pos.x - center.x + center.y);
        }
        else if (dir == ROTATE_DIRECTION.Right)
        {
            return new Vector2Int(pos.y - center.y + center.x, center.x - pos.x + center.y);
        }
        else if (dir == ROTATE_DIRECTION.Horizon)
        {
            return new Vector2Int(center.x - (pos.x - center.x), pos.y);
        }
        else if (dir == ROTATE_DIRECTION.Vertical)
        {
            return new Vector2Int(pos.x, center.y - (pos.y - center.y));
        }
        return Vector2Int.zero;
    }

    public static Vector2Int RotatePos(Vector2Int pos, Vector2Int center, Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return pos;
            case Direction.Right:
                return new Vector2Int(pos.y - center.y + center.x, center.x - pos.x + center.y);
            case Direction.Down:
                return center - (pos - center);
            case Direction.Left:
                return new Vector2Int(center.y - pos.y + center.x, pos.x - center.x + center.y);
        }
        return Vector2Int.zero;
    }
}

