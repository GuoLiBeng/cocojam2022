﻿using UnityEngine;
using System.Collections;

namespace UnityEngine.UI
{
    public interface ILoopScrollItem
    {
        void ScrollCellContent(object o);
    }

    public abstract class LoopScrollDataSource
    {
        public abstract void ProvideData(Transform transform, int idx);
    }

    public class LoopScrollSendIndexSource : LoopScrollDataSource
    {
        public static readonly LoopScrollSendIndexSource Instance = new LoopScrollSendIndexSource();

        LoopScrollSendIndexSource() { }

        public override void ProvideData(Transform transform, int idx)
        {
            transform.SendMessage("ScrollCellIndex", idx);
        }
    }

    public class LoopScrollArraySource<T> : LoopScrollDataSource
    {
        T[] objectsToFill;

        public LoopScrollArraySource(T[] objectsToFill)
        {
            this.objectsToFill = objectsToFill;
        }

        public override void ProvideData(Transform transform, int idx)
        {
            var loopScrollItem = transform.GetComponent<ILoopScrollItem>();
            if (loopScrollItem != null)
            {
                loopScrollItem.ScrollCellContent(objectsToFill[idx]);
                return;
            }
            transform.SendMessage("ScrollCellContent", objectsToFill[idx]);
        }
    }
}