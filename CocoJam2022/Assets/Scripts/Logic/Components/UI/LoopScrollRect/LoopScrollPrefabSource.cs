﻿using UnityEngine;
using System.Collections;
using ObjectPool;

namespace UnityEngine.UI
{
    [System.Serializable]
    public class LoopScrollPrefabSource
    {
        public string prefabName;
        public int poolSize = 5;

        private bool inited = false;
        public virtual GameObject GetObject()
        {
            return ObjectPoolManager.instance.UIPool.Pop("Prefabs/UI/" + prefabName);
        }

        public virtual void ReturnObject(Transform go)
        {
            go.SendMessage("ScrollCellReturn", SendMessageOptions.DontRequireReceiver);
            ObjectPoolManager.instance.UIPool.Push("Prefabs/UI/" + prefabName, go.gameObject);
        }
    }
}
