﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic
{
    public class MultiItemManager : MonoBehaviour
    {
        [SerializeField] GameObject m_itemModel;
        [SerializeField] GameObject[] showOnEmpty;
        [SerializeField] GameObject[] showOnNonEmpty;

        private int m_lastSize = 0;
        private List<GameObject> m_list = new List<GameObject>();
        private bool m_init = false;

        private void ForceRefreshView(GameObject obj)
        {
            var grp = obj.GetComponentInParent<UnityEngine.UI.Graphic>();
            if (grp != null)
                grp.SetAllDirty();
        }

        public void ReInit()
        {
            foreach (var obj in m_list)
                Destroy(obj);
            m_list.Clear();
            m_lastSize = 0;
            m_init = false;
        }

        private void Init()
        {
            if (!m_init)
            {
                m_init = true;

                Assert.IsNotNull(m_itemModel);
                m_itemModel.SetActive(false);
                RefreshReferencedObjects();
            }
        }

        void RefreshReferencedObjects()
        {
            foreach (var gameObject in showOnEmpty)
                if (gameObject)
                    gameObject.SetActive(m_lastSize <= 0);
            foreach (var gameObject in showOnNonEmpty)
                if (gameObject)
                    gameObject.SetActive(m_lastSize > 0);
        }

        public int Size => m_lastSize;

        public IEnumerable<GameObject> IterItems
        {
            get
            {
                var size = Size;
                for (var i = 0; i < size; i++)
                    yield return m_list[i];
            }
        }

        /// <summary>
        /// 设置List的长度
        /// </summary>
        /// <param name="size"></param>
        public void SetSize(int size)
        {
            Init();
            var root = m_itemModel.transform.parent;

            while (m_list.Count < size)
            {
                var obj = GameObject.Instantiate(m_itemModel, root);
                obj.name = m_itemModel.name + m_list.Count.ToString();
                m_list.Add(obj);
            }

            for (var i = m_lastSize; i < size; i++)
                m_list[i].SetActive(true);
            for (var i = size; i < m_list.Count; i++)
                m_list[i].SetActive(false);

            if (m_lastSize < size)
                ForceRefreshView(root.gameObject);

            m_lastSize = size;

            RefreshReferencedObjects();
        }

        /// <summary>
        /// 扩展1个大小
        /// </summary>
        /// <remarks>这对于未知长度的枚举相当适用:foreach(var item in enumable) Expand();</remarks>
        /// <returns></returns>
        public GameObject Expand()
        {
            SetSize(m_lastSize + 1);
            return GetItem(m_lastSize - 1);
        }

        /// <summary>
        /// 获取List中第N个Item
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public GameObject GetItem(int index)
        {
            Init();
            return m_list[index];
        }

        public int GetItemIndex(GameObject obj)
        {
            if (m_itemModel != null)
            {
                if (int.TryParse(obj.name.Substring(m_itemModel.name.Length), out int index))
                {
                    return index;
                }
            }
            return -1;
        }

        public T GetItem<T>(int index) where T : Component
        {
            return GetItem(index).GetComponent<T>();
        }
    }
}

