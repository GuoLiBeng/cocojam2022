using System;
using UnityEngine;
using Config;

namespace Logic
{
    public class ClickableObj : MonoBehaviour
    {
        public static ClickableObj Get(GameObject gameObject)
        {
            var clickToMove = gameObject.GetComponent<ClickableObj>();
            if (clickToMove == null) clickToMove = gameObject.AddComponent<ClickableObj>();
            return clickToMove;
        }

        public static void Set(GameObject obj, Action<GameObject> clickCallback, string tips = "", bool highLight = true)
        {
            var clickableObj = Get(obj);
            clickableObj.Init(clickCallback, tips, highLight);
        }
        public static void Set(GameObject obj, Action<GameObject> clickCallback, Func<string> tipsFunc, bool highLight = true)
        {
            var clickableObj = Get(obj);
            clickableObj.Init(clickCallback, tipsFunc, highLight);
        }

        public static void Set(GameObject obj, Action<GameObject> clickCallback, EventItemConfig config, bool highLight = true)
        {
            var clickableObj = Get(obj);
            clickableObj.Init(clickCallback, config, highLight);
        }

        public static void Remove(GameObject obj)
        {
            var clickToMove = obj.GetComponent<ClickableObj>();
            if (clickToMove != null)
            {
                GameObject.Destroy(clickToMove);
            }
        }

        private string tips;
        private Func<string> tipsFunc;
        private EventItemConfig config;
        private Action<GameObject> clickCallback;
        private bool isHighLight;
        private ItemData itemData;

        void Awake()
        {
            var eventTriggerListener = EventTriggerListener.Get(gameObject);
            eventTriggerListener.onEnter += OnEnter;
            eventTriggerListener.onExit += OnExit;
            eventTriggerListener.onClick += OnClick;
        }

        void OnDisable()
        {
            HighLight.Set(gameObject, false);
            TipsController.instance.HideMouseTips();
            TipsController.instance.HideEventInfo();
        }

        void OnDestroy()
        {
            var eventTriggerListener = EventTriggerListener.Get(gameObject);
            eventTriggerListener.onEnter -= OnEnter;
            eventTriggerListener.onExit -= OnExit;
            eventTriggerListener.onClick -= OnClick;
        }

        public void Init(Action<GameObject> clickCallback, string tips = "", bool highLight = true)
        {
            this.clickCallback = clickCallback;
            this.tips = tips;
            this.tipsFunc = null;
            this.config = null;
            this.isHighLight = highLight;
        }

        public void Init(Action<GameObject> clickCallback, Func<string> tipsFunc, bool highLight = true)
        {
            this.clickCallback = clickCallback;
            this.tips = null;
            this.tipsFunc = tipsFunc;
            this.config = null;
            this.isHighLight = highLight;
        }

        public void Init(Action<GameObject> clickCallback, EventItemConfig config, bool highLight = true)
        {
            this.clickCallback = clickCallback;
            this.tips = null;
            this.tipsFunc = null;
            this.config = config;
            this.isHighLight = highLight;
        }

        private void OnEnter(GameObject gameObject)
        {
            if (isHighLight)
            {
                HighLight.Set(gameObject, true);
            }

            string tipString = tipsFunc == null ? tips : tipsFunc();
            if (!string.IsNullOrEmpty(tipString))
            {
                TipsController.instance.ShowMouseTips(tipString);
            }
            else if (config != null)
            {
                TipsController.instance.ShowEventInfo(config);
            }
        }

        private void OnExit(GameObject gameObject)
        {
            HighLight.Set(gameObject, false);
            TipsController.instance.HideMouseTips();
            TipsController.instance.HideEventInfo();
        }

        private void OnClick(GameObject gameObject)
        {
            if (clickCallback != null)
            {
                AudioManager.instance.PlayEffect("UI_switch");
                clickCallback.Invoke(gameObject);
            }
        }
    }
}