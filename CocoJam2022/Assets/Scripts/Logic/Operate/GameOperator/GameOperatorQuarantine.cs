using Config;
using UnityEngine.UI;

namespace Logic
{
    public class GameOperatorQuarantine : GameOperatorBaseEvent
    {
        public override eGameOperatorState state => eGameOperatorState.Quarantine;
        public GameOperatorQuarantine(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Quarantine);
        }

        public override void OnGetCommonEventUIResult(EventParam.CommonEventUIResultParam param)
        {
            PlayerController.instance.model.ForceSetValue(config);
            GameController.instance.model.SpendTime(config.costTime);
            TipsController.instance.OpenSelectUI2(
                "隔离结束,终于回来了~",
                1,
                (index, obj) =>
                {
                    obj.GetComponentInChildren<Text>().text = "太开心了";
                },
                (index, obj) =>
                {
                    TipsController.instance.HideSelectUI2();
                    ChangeState(eGameOperatorState.Empty);
                }
            );
        }
    }
}