using Config;
using UnityEngine;

namespace Logic
{
    public class GameOperatorEmpty : BaseOperator<eGameOperatorState>
    {
        public override eGameOperatorState state => eGameOperatorState.Empty;
        private bool todayPlayIntro = false;
        private float introCounter = 0;
        public GameOperatorEmpty(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            EventManager.Get<EventDefine.TimeUpdate>().AddListener((param) =>
            {
                if (param.crossDay)
                {
                    todayPlayIntro = false;
                }
            });
        }
        public override bool CanEnter()
        {
            return true;
        }
        public override bool CanBreak(eGameOperatorState newState)
        {
            return true;
        }

        public override void Enter()
        {
            if (!todayPlayIntro && AudioManager.instance.radioEnable)
            {
                AudioManager.instance.PlayBGM("bgm_init_intro");
            }
            else
            {
                AudioManager.instance.StopBGM();
            }
        }

        public override void Exit()
        {
            todayPlayIntro = true;
            AudioManager.instance.StopBGM();
        }

        public override void Update()
        {
            if (introCounter < 26)
            {
                introCounter += Time.deltaTime;
                if (introCounter >= 26)
                {
                    todayPlayIntro = true;
                    AudioManager.instance.StopBGM();
                }
            }
        }
    }
}