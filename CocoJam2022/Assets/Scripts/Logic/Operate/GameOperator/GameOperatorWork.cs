using Config;
namespace Logic
{
    public class GameOperatorWork : GameOperatorBaseEvent
    {
        public override eGameOperatorState state => eGameOperatorState.Work;
        public GameOperatorWork(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Work);
        }

        public override void Enter()
        {
            base.Enter();
            AudioManager.instance.PlayAGM("工作_loop");
        }

        public override void Exit()
        {
            AudioManager.instance.StopAGM();
            AudioManager.instance.PlayEffect("工作_结束");
            base.Exit();
        }
    }
}