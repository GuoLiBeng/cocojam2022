using Config;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class GameOperatorFree : BaseOperator<eGameOperatorState>
    {

        public GameOperatorFree(OperateMachine<eGameOperatorState> machine) : base(machine)
        {

        }

        public override eGameOperatorState state => eGameOperatorState.Free;

        public override bool CanBreak(eGameOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Enter()
        {
            GameController.instance.freeUI.Show();
        }

        public override void Exit()
        {
            GameController.instance.freeUI.Hide();
        }

        public override void Update()
        {

        }
    }
}