using Config;
using UnityEngine;

namespace Logic
{
    public abstract class GameOperatorBaseEvent : BaseOperator<eGameOperatorState>
    {
        public virtual EventItemConfig config { get; set; }
        protected int ExecuteTimeToday = 0;
        public GameOperatorBaseEvent(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            EventManager.Get<EventDefine.TimeUpdate>().AddListener(OnTimeUpdate);
        }
        public override bool CanEnter()
        {
            if (config.oneDayTimes > 0 && ExecuteTimeToday >= config.oneDayTimes)
            {
                TipsController.instance.ShowTips($"{config.eventName}已达到每日次数上限{config.oneDayTimes}次");
                return false;
            }
            (int h, int m) = StaticUtils.MinuteToHour(GameController.instance.model.currentTime);
            if (h < config.limitMinHour || h > config.limitMaxHour)
            {
                TipsController.instance.ShowTips($"{config.eventName}只能在{config.limitMinHour}点到{config.limitMaxHour}点之间执行");
                return false;
            }

            return true;
        }
        public override bool CanBreak(eGameOperatorState newState)
        {
            //TODO  写统一的跳出逻辑
            return true;
        }

        public override void Enter()
        {
            ExecuteTimeToday++;
            GameController.instance.commonEventUI.Show(config);
            EventManager.Get<EventDefine.CommonEventUIResult>().AddListener(OnGetCommonEventUIResult);
        }

        public override void Exit()
        {
            EventManager.Get<EventDefine.CommonEventUIResult>().RemoveListener(OnGetCommonEventUIResult);
            GameController.instance.commonEventUI.Hide();
        }

        private void OnTimeUpdate(EventParam.TimeUpdateParam param)
        {
            if (param.crossDay)
            {
                ExecuteTimeToday = 0;
            }
        }
        public override void Destroy()
        {
            EventManager.Get<EventDefine.TimeUpdate>().RemoveListener(OnTimeUpdate);
            base.Destroy();
        }

        public override void Update()
        {

        }

        public virtual void OnGetCommonEventUIResult(EventParam.CommonEventUIResultParam param)
        {
            PlayerController.instance.model.ApplyEffect(config, param.valueScale);
            GameController.instance.model.SpendTime((int)(config.costTime * param.completePercent));
            ChangeState(eGameOperatorState.Empty);
        }
    }

    public abstract class GameOperatorBaseEvent<T> : BaseOperator<eGameOperatorState, T>
    {
        public virtual EventItemConfig config { get; set; }
        protected int ExecuteTimeToday = 0;
        protected GameOperatorBaseEvent(OperateMachine<eGameOperatorState> machine) : base(machine)
        {

        }
        public override bool CanEnter(T param)
        {
            if (config.oneDayTimes > 0 && ExecuteTimeToday < config.oneDayTimes)
            {
                TipsController.instance.ShowTips($"已达到每日次数上限{config.oneDayTimes}次");
                return false;
            }
            (int h, int m) = StaticUtils.MinuteToHour(GameController.instance.model.currentTime);
            if (h < config.limitMinHour || h >= config.limitMaxHour)
            {
                TipsController.instance.ShowTips($"只能在{config.limitMinHour}点到{config.limitMaxHour}点之间执行");
                return false;
            }

            return true;
        }
        public override bool CanBreak(eGameOperatorState newState)
        {
            //TODO  写统一的跳出逻辑
            return true;
        }

        public override void Enter(T param)
        {
            ExecuteTimeToday++;
            GameController.instance.commonEventUI.Show(config);
            EventManager.Get<EventDefine.CommonEventUIResult>().AddListener(OnGetCommonEventUIResult);
        }

        public override void Exit()
        {
            EventManager.Get<EventDefine.CommonEventUIResult>().RemoveListener(OnGetCommonEventUIResult);
            GameController.instance.commonEventUI.Hide();
        }

        private void OnTimeUpdate(EventParam.TimeUpdateParam param)
        {
            if (param.crossDay)
            {
                ExecuteTimeToday = 0;
            }
        }
        public override void Destroy()
        {
            EventManager.Get<EventDefine.TimeUpdate>().RemoveListener(OnTimeUpdate);
            base.Destroy();
        }

        public override void Update()
        {

        }

        public virtual void OnGetCommonEventUIResult(EventParam.CommonEventUIResultParam param)
        {
            PlayerController.instance.model.ApplyEffect(config, param.valueScale);
            GameController.instance.model.SpendTime((int)(config.costTime * param.completePercent));
            ChangeState(eGameOperatorState.Empty);
        }
    }
}