using Config;
namespace Logic
{
    public class GameOperatorRest : GameOperatorBaseEvent<EventItemConfig>
    {
        public override eGameOperatorState state => eGameOperatorState.Rest;
        public GameOperatorRest(OperateMachine<eGameOperatorState> machine) : base(machine)
        {

        }

        public override bool CanEnter(EventItemConfig param)
        {
            config = param;
            return base.CanEnter(param);
        }

        public override void Enter(EventItemConfig param)
        {
            config = param;
            base.Enter(param);
            if (config.eventName == "玩手机")
            {
                AudioManager.instance.PlaySingletonEffect("玩手机loop", true);
            }
        }

        public override void Exit()
        {
            if (config.eventName == "玩手机")
            {
                AudioManager.instance.StopSingletonEffect("玩手机loop");
            }
            base.Exit();
        }
    }
}