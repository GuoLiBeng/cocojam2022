using Config;
namespace Logic
{
    public class GameOperatorSettleRes : GameOperatorBaseEvent
    {
        public override eGameOperatorState state => eGameOperatorState.SettleRes;
        public GameOperatorSettleRes(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.SettleRes);
        }

        public override void Enter()
        {
            ExecuteTimeToday++;
            RefrigeratorController.instance.ShowUI();
            PlayerController.instance.model.ApplyEffect(config);
            EventManager.Get<EventDefine.SettleResFinished>().AddListener(OnSettleResFinished);
        }

        public override void Exit()
        {
            EventManager.Get<EventDefine.SettleResFinished>().RemoveListener(OnSettleResFinished);
            RefrigeratorController.instance.HideUI();
        }

        private void OnSettleResFinished()
        {
            PlayerModel.instance.AddAbilityExp(eAbilityType.Settle, 20);
            GameController.instance.model.SpendTime(config.costTime);
            ChangeState(eGameOperatorState.Empty);
        }
    }
}