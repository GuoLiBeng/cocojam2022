namespace Logic
{
    public enum eGameOperatorState
    {
        FirstEnter,
        Empty,
        Cook,
        Work,
        Sleep,
        Buy,
        SettleRes,
        Rest,
        Play,
        Quarantine,
        Free,
    }
}