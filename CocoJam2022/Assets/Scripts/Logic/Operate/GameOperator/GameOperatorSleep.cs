using Config;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class GameOperatorSleep : GameOperatorBaseEvent
    {
        public GameOperatorSleep(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Sleep);
        }
        public override eGameOperatorState state => eGameOperatorState.Sleep;

        public override void Enter()
        {
            base.Enter();
            AudioManager.instance.PlayEffect("躺下");
        }

        public override void Exit()
        {
            AudioManager.instance.PlayEffect("起身");
            base.Exit();
        }

        public override void OnGetCommonEventUIResult(EventParam.CommonEventUIResultParam param)
        {
            PlayerController.instance.model.ApplyEffect(config, param.valueScale);
            GameController.instance.model.ForceSetTime(6, 0);
            if (GameModel.instance.TryRollFree())
            {
                GameController.instance.ChangeState(eGameOperatorState.Free);
            }
            else if (PlayerModel.instance.TryRollInfection())
            {
                TipsController.instance.OpenSelectUI2(
                    "很遗憾的通知您  您被感染了  你要被拉到方舱隔离14天",
                    1,
                    (index, obj) =>
                    {
                        obj.GetComponentInChildren<Text>().text = "被拉去隔离";
                    },
                    (index, obj) =>
                    {
                        TipsController.instance.HideSelectUI2();
                        GameController.instance.ChangeState(eGameOperatorState.Quarantine);
                    }
                );
            }
            else
            {
                ChangeState(eGameOperatorState.Buy);
            }
        }
    }
}