using Config;
using UnityEngine;
namespace Logic
{
    public class GameOperatorBuy : GameOperatorBaseEvent
    {
        public override eGameOperatorState state => eGameOperatorState.Buy;
        private int ExcuteTimeToday = 0;
        public GameOperatorBuy(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Buy);
        }

        public override void Exit()
        {
            EventManager.Get<EventDefine.MorningBugResult>().RemoveListener(OnGetBugResult);
            PhoneController.instance.HideUI(ePhoneUI.Buy);
        }

        public override void Enter()
        {
            AudioManager.instance.PlayBGM("抢菜beat_lp");
            ExecuteTimeToday++;
            TipsController.instance.ShowTips("叮叮叮~~叮叮叮~~   又要抢菜了");
            PhoneController.instance.ShowUI(ePhoneUI.Buy);
            EventManager.Get<EventDefine.MorningBugResult>().AddListener(OnGetBugResult);
        }

        public void OnGetBugResult(bool param)
        {
            GameController.instance.model.SpendTime(config.costTime);
            if (!param)
            {
                EventItemConfig cloneConfig = new EventItemConfig();
                cloneConfig.changeEMO = config.changeEMO;
                cloneConfig.changeHealth = config.changeHealth;
                cloneConfig.changeInfection = config.changeInfection;
                PlayerController.instance.model.ApplyEffect(cloneConfig);
            }
            else
            {
                PlayerController.instance.model.ApplyEffect(config);
            }
            ChangeState(eGameOperatorState.Empty);
        }
    }
}