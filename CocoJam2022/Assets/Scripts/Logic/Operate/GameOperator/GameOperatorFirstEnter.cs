using Config;
namespace Logic
{
    public class GameOperatorFirstEnter : BaseOperator<eGameOperatorState>
    {
        public override eGameOperatorState state => eGameOperatorState.FirstEnter;
        public GameOperatorFirstEnter(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
        }

        public override bool CanBreak(eGameOperatorState newState)
        {
            return true;
        }
        public override bool CanEnter()
        {
            return true;
        }
        public override void Enter()
        {
            GameController.instance.model.ForceSetTime(6, 0);
            GameController.instance.ShowIntroUI();
        }

        public override void Exit()
        {
            GameController.instance.HideIntroUI();
        }

        public override void Update()
        {

        }
    }
}