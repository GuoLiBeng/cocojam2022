using System.Collections.Generic;
using Config;
using UnityEngine;

namespace Logic
{
    public class GameOperatorCook : GameOperatorBaseEvent<MenuConfig>
    {
        public override eGameOperatorState state => eGameOperatorState.Cook;
        private MenuConfig menuConfig;
        public GameOperatorCook(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Cook);
        }

        public override void Enter(MenuConfig param)
        {
            ExecuteTimeToday++;
            menuConfig = param;
            CookController.instance.ShowCookGameUI();
            EventManager.Get<EventDefine.CookFinished>().AddListener(OnGetCookResult);
            AudioManager.instance.PlayBGM("施斌_苏州评弹");
            AudioManager.instance.PlayAGM("做饭_翻炒loop");
            AudioManager.instance.PlayEffect("做饭_开火");
        }

        public override void Exit()
        {
            AudioManager.instance.PlayEffect("做饭_熄火");
            AudioManager.instance.StopAGM();
            AudioManager.instance.StopBGM();
            EventManager.Get<EventDefine.CookFinished>().RemoveListener(OnGetCookResult);
            CookController.instance.HideCookGameUI();
        }

        public void OnGetCookResult(EventParam.CookFinishedParam param)
        {
            GameModel.instance.SpendTime((int)(menuConfig.costTime * param.completePercent));
            Dictionary<string, int> itemDict = new Dictionary<string, int>();
            foreach (var item in menuConfig.materials)
            {
                RefrigeratorModel.instance.ConsumeItem(item.itemName, item.amount);
                if (itemDict.ContainsKey(item.itemName))
                {
                    itemDict[item.itemName] += item.amount;
                }
                else
                {
                    itemDict.Add(item.itemName, item.amount);
                }
            }
            if (itemDict.Count > 0)
            {
                string log = "消耗物资:";
                int count = 0;
                foreach (var pairs in itemDict)
                {
                    count++;
                    log += $"{pairs.Key}×{pairs.Value}";
                    if (count < itemDict.Count)
                    {
                        log += "、";
                    }
                }
                TipsController.instance.ShowTips(log);
            }
            PlayerController.instance.model.ApplyEffect(menuConfig, param.scaleValue);
            ChangeState(eGameOperatorState.Empty);
        }
    }
}