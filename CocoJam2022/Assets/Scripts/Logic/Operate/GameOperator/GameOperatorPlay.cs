using Config;
namespace Logic
{
    public class GameOperatorPlay : GameOperatorBaseEvent
    {
        public override eGameOperatorState state => eGameOperatorState.Play;
        public GameOperatorPlay(OperateMachine<eGameOperatorState> machine) : base(machine)
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Play);
        }

        public override void Enter()
        {
            AudioManager.instance.PlayAGM("娱乐loop");
            base.Enter();
        }

        public override void Exit()
        {
            AudioManager.instance.StopAGM();
            base.Exit();
        }
    }
}