using UnityEngine;
using System;
using Config;

namespace Logic
{
    namespace EventDefine
    {
        public class ModulesInitFinished : Event { };
        public class TimeUpdate : Event<EventParam.TimeUpdateParam> { };
        public class MorningBugResult : Event<bool> { };
        public class WorkFinished : Event<EventParam.WorkFinishedParam> { };
        public class PlayerValueChanged : Event { };
        public class CookFinished : Event<EventParam.CookFinishedParam> { };
        public class SettleResFinished : Event { };
        public class RefrigeratorHint : Event<EventParam.RefrigeratorHint> { };
        public class RefrigeratorObstacleUpdate : Event { };
        public class EmergencyFinished : Event { };
        public class AbilityUpgrade : Event<eAbilityType, int> { };
        public class CommonEventUIResult : Event<EventParam.CommonEventUIResultParam> { };
        public class BeatItemResult : Event<EventParam.BeatItemParam> { };
        public class CookGameResult : Event<bool> { }
    }

    namespace EventParam
    {
        public class TimeUpdateParam
        {
            public int SpendTime;
            public int currentTime;
            public bool crossDay;
        }

        public class WorkFinishedParam
        {
            public bool success;
        }

        public class CookFinishedParam
        {
            public float completePercent;
            public float scaleValue;

        }

        public class RefrigeratorHint
        {
            public Vector2Int[] gridPos;
            public Color color;
        }

        public class CommonEventUIResultParam
        {
            public bool haveEmergency;
            public EventItemConfig emergencyConfig;
            public float completePercent;
            public bool isContinue;
            public float valueScale;
        }

        public class BeatItemParam
        {
            public int index;
            public bool success;
            public float scale;
            public BeatResultType type;
        }
        public enum BeatResultType
        {
            Perfect,
            Good,
            Bad,
            Miss,
        }
    }
}
