﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    //UI点击处理
    public class ClickHandler : MonoBehaviour
    {
        public static ClickHandler Get(GameObject go)
        {
            ClickHandler handler = go.GetComponent<ClickHandler>();
            if (handler == null) handler = go.AddComponent<ClickHandler>();
            return handler;
        }
        public Action<GameObject> onClick;

        [HideInInspector]
        private Button btn;

        private void Start()
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if (onClick != null) onClick(gameObject);
        }
    }
}