namespace Logic
{
    public interface IModel
    {
        void Init();
        void Destroy();
    }
}