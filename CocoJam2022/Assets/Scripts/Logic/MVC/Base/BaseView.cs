using UnityEngine;
using ObjectPool;
using UnityEngine.UI;

namespace Logic
{
    public abstract class BaseView : IView
    {
        public bool isShow => sceneObject == null ? false : sceneObject.activeSelf;
        public bool isInit => sceneObject != null;
        public GameObject sceneObject;
        protected abstract string prefabName { get; }
        public void Init()
        {
            if (isInit) return;
            CreateSceneObject();
            RegisterClicks();
            RegisterEvents();
            OnInit();
        }

        protected abstract void CreateSceneObject();

        protected abstract void OnInit();

        protected abstract void RegisterClicks();
        protected abstract void UnRegisterClicks();

        protected virtual void RegisterEvents() { }
        protected virtual void UnRegisterEvents() { }

        public void Show()
        {
            if (!isInit || isShow) return;
            sceneObject.SetActive(true);
            OnShow();
        }

        protected abstract void OnShow();

        public void Hide()
        {
            if (!isInit || !isShow) return;
            OnHide();
            sceneObject.SetActive(false);
        }

        protected abstract void OnHide();
        protected abstract void OnClick(GameObject obj);

        public void Destroy()
        {
            if (!isInit) return;
            if (isShow) Hide();
            OnDestroy();
            if (sceneObject != null)
            {
                UnRegisterClicks();
                UnRegisterEvents();
                DestorySceneObject();
            }
        }

        protected abstract void DestorySceneObject();

        protected abstract void OnDestroy();

        protected T GetChild<T>(string path) where T : Component
        {
            var obj = sceneObject.transform.Find(path);
            if (obj != null)
            {
                return obj.GetComponent<T>();
            }
            Debug.LogError($"{sceneObject.name}未找到{path}", sceneObject);
            return null;
        }

        protected GameObject GetChild(string path)
        {
            return sceneObject.transform.Find(path).gameObject;
        }
    }
}