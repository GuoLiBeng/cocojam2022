namespace Logic
{
    public class ModuleManager : SingletonClass<ModuleManager>
    {
        public void InitAllModules()
        {
            InputController.instance.Init();
            GameController.instance.Init();
            RoomController.instance.Init();
            PhoneController.instance.Init();
            WorkController.instance.Init();
            PlayerController.instance.Init();
            CookController.instance.Init();
            RefrigeratorController.instance.Init();
            SleepController.instance.Init();
            DebugController.instance.Init();
            TipsController.instance.Init();

            EventManager.Get<EventDefine.ModulesInitFinished>().Fire();
        }

        public void Update()
        {
            GameController.instance.Update();
            RefrigeratorController.instance.Update();
            PhoneController.instance.Update();
            DebugController.instance.Update();
            InputController.instance.Update();
            RoomController.instance.Update();
        }

        public void FixedUpdate()
        {
            RefrigeratorController.instance.FixedUpdate();
            TipsController.instance.FixedUpdate();
        }

        public void DestroyAllModules()
        {
            InputController.instance.Destroy();
            SleepController.instance.Destroy();
            DebugController.instance.Destroy();
            RefrigeratorController.instance.Destroy();
            CookController.instance.Destroy();
            PlayerController.instance.Destroy();
            WorkController.instance.Destroy();
            TipsController.instance.Destroy();
            PhoneController.instance.Destroy();
            RoomController.instance.Destroy();
            GameController.instance.Destroy();
        }
    }
}