using UnityEngine;
using ObjectPool;
using System.Collections.Generic;

namespace Logic
{
    public abstract class BaseObject : BaseView
    {
        public virtual Vector3 initPos => Vector3.zero;

        protected sealed override void RegisterClicks()
        {
            var collider2Ds = sceneObject.transform.GetComponentsInChildren<Collider2D>(true);
            foreach (var collider2D in collider2Ds)
            {
                var eventTriggerListener = EventTriggerListener.Get(collider2D.gameObject);
                eventTriggerListener.onClick += OnClick;
                eventTriggerListener.onEnter += OnEnter;
                eventTriggerListener.onExit += OnExit;
            }
        }

        protected sealed override void UnRegisterClicks()
        {
            var collider2Ds = sceneObject.transform.GetComponentsInChildren<Collider2D>(true);
            foreach (var collider2D in collider2Ds)
            {
                var eventTriggerListener = EventTriggerListener.Get(collider2D.gameObject);
                eventTriggerListener.onClick -= OnClick;
                eventTriggerListener.onEnter -= OnEnter;
                eventTriggerListener.onExit -= OnExit;
            }
        }

        protected override void CreateSceneObject()
        {
            sceneObject = ObjectPoolManager.instance.ObjectPool.Pop(prefabName);
            ViewManager.instance.AddObject(sceneObject, initPos);
        }

        protected override void DestorySceneObject()
        {
            ObjectPoolManager.instance.ObjectPool.Push(prefabName, sceneObject);
            sceneObject = null;
        }

        protected virtual void OnEnter(GameObject obj)
        {

        }

        protected virtual void OnExit(GameObject obj)
        {

        }
    }
}