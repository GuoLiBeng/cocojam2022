namespace Logic
{
    public interface IView
    {
        void Init();
        void Show();
        void Hide();
        void Destroy();
    }
}