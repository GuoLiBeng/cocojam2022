using UnityEngine;

namespace Logic
{
    public class ViewManager : SingletonMonoBehaviourClass<ViewManager>
    {
        private Transform m_uiRoot;
        public Transform UIRoot => m_uiRoot ??= transform.Find("UIRoot");

        private Transform m_ObjectRoot;
        public Transform ObjectRoot => m_ObjectRoot ??= transform.Find("ObjectRoot");

        public void AddUI(GameObject ui, UISort sort)
        {
            ui.transform.SetParent(UIRoot);
            RectTransform rect = ui.transform as RectTransform;
            rect.anchoredPosition3D = new Vector3(0, 0, GetZOrder(sort));
            for (int i = 0; i < UIRoot.childCount; i++)
            {
                var child = UIRoot.GetChild(i);
                if (rect.anchoredPosition3D.z < (child as RectTransform).anchoredPosition3D.z)
                {
                    ui.transform.SetSiblingIndex(i);
                    break;
                }
            }
        }

        private int GetZOrder(UISort sort)
        {
            return (int)sort * 10;
        }

        public void AddObject(GameObject obj, Vector3 pos)
        {
            obj.transform.SetParent(ObjectRoot);
            obj.transform.localPosition = pos;
        }
    }
}