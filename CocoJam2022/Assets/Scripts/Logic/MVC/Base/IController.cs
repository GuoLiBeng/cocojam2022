namespace Logic
{
    public interface IController
    {
        void Init();
        void Destroy();
    }

    public interface IController<T> : IController where T : IModel
    {
        T model { get; }
    }
}