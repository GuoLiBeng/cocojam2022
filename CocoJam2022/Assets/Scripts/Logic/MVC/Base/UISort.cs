namespace Logic
{
    //序号越高  在UI里的层级越高
    public enum UISort
    {
        MainUI,
        PhoneBuyUI,
        RoomUI,
        WorkUI,
        CookUI,
        RefrigeratorUI,
        ItemUI,
        IntroUI,
        EmergencyUI,
        GameUI,
        FreeUI,

        #region Tips
        SelectUI,
        SelectUI2,
        EventInfoUI,
        MouseTipsUI,
        ItemInfoUI,
        TipsUI,
        #endregion
    }
}