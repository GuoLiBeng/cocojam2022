using UnityEngine;

namespace Logic
{
    public class PlayerUI : BaseUI
    {
        protected override string prefabName => throw new System.NotImplementedException();

        protected override UISort sort => throw new System.NotImplementedException();

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }
    }
}