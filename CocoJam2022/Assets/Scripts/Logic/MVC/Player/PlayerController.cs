namespace Logic
{
    public class PlayerController : SingletonClass<PlayerController>, IController<PlayerModel>
    {
        public PlayerModel model => PlayerModel.instance;
        public void Init()
        {
            model.Init();
        }
        public void Destroy()
        {
            model.Destroy();
        }
    }
}