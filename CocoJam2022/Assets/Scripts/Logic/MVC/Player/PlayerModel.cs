using Config;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Logic
{
    public class PlayerModel : SingletonClass<PlayerModel>, IModel
    {
        #region  基础四属性
        private float m_infection = 0;
        public float infection
        {
            get => m_infection;
            private set { m_infection = Mathf.Clamp01(value); }
        }
        private int m_emo = 0;
        public int emo { get => m_emo; private set { m_emo = Mathf.Clamp(value, 0, config.maxEmo); } }
        private int m_health = 0;
        public int health { get => m_health; private set { m_health = Mathf.Clamp(value, 0, config.maxHealth); } }
        public int money { get; private set; } = 0;
        private int bankMoney = 0;
        private int nextPayMoneyDay = 10;
        #endregion

        #region 能力
        public Dictionary<eAbilityType, int> abilityLevel;
        public Dictionary<eAbilityType, int> abilityExp;
        #endregion

        private PlayerConfig config => ConfigManager.instance.playerConfig;
        public void Init()
        {
            infection = config.infection;
            emo = config.emo;
            health = config.health;
            money = config.money;

            abilityLevel = new Dictionary<eAbilityType, int>();
            abilityLevel.Add(eAbilityType.Settle, 0);
            abilityLevel.Add(eAbilityType.Cook, 0);
            abilityExp = new Dictionary<eAbilityType, int>();

            EventManager.Get<EventDefine.TimeUpdate>().AddListener(OnTimeUpdate);
        }
        public void Destroy()
        {
            EventManager.Get<EventDefine.TimeUpdate>().RemoveListener(OnTimeUpdate);
        }

        // public void ChangeInfection(float value)
        // {
        //     var oldInfection = infection;
        //     infection += value;
        //     if (infection != oldInfection) EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
        // }

        // public void ChangeEMO(int value)
        // {
        //     var oldEmo = emo;
        //     emo += value;
        //     if (oldEmo != emo) EventManager.Get<EventDefine.PlayerValueChanged>().Fire();

        // }

        // public void ChangeHealth(int value)
        // {
        //     var oldHealth = health;
        //     health += value;
        //     if (oldHealth != health) EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
        // }

        // public void ChangeMoney(int value)
        // {
        //     //金钱不存在数量限制  也可以是负的
        //     if (value > 0)
        //     {
        //         //先暂存起来  到发薪日再加上
        //         bankMoney += value;
        //     }
        //     else
        //     {
        //         money += value;
        //         EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
        //     }
        // }
        public void ApplyEffect(IEffect effect, float scale = 1.0f)
        {
            float oldInfection = infection;
            int oldEmo = emo;
            int oldHealth = health;
            int oldMoney = money;
            infection += effect.changeInfection;
            infection += Mathf.Abs(effect.changeInfection) * (1 - scale);
            emo += effect.changeEMO;
            emo += Mathf.FloorToInt(Mathf.Abs(effect.changeEMO) * (1 - scale));
            health += effect.changeHealth;
            health -= Mathf.FloorToInt(Mathf.Abs(effect.changeHealth) * (1 - scale));
            if (effect.changeMoney > 0)
            {
                //先暂存起来  到发薪日再加上
                bankMoney += effect.changeMoney;
            }
            else
            {
                AudioManager.instance.PlayEffect("金钱_减少");
                money += effect.changeMoney;
            }
            string tip = "";

            if (health != oldHealth)
            {
                float change = health - oldHealth;
                tip += $"体力{(change > 0 ? "+" : "")}{change}  ";
            }
            if (money != oldMoney)
            {
                float change = money - oldMoney;
                tip += $"金钱{(change > 0 ? "+" : "")}{change}  ";
            }
            if (emo != oldEmo)
            {
                float change = oldEmo - emo;
                tip += $"SAN值{(change > 0 ? "+" : "")}{change}  ";
            }
            if (infection != oldInfection)
            {
                float change = infection - oldInfection;
                tip += $"感染概率{(change > 0 ? "+" : "")}{(change * 100)}%  ";
            }
            if (!string.IsNullOrEmpty(tip))
            {
                TipsController.instance.ShowTips(tip);
            }
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
            if (health <= 0)
            {
                TipsController.instance.OpenSelectUI2(
                    "健康值为0, 只能去睡一会儿了",
                    1,
                    (index, obj) =>
                    {
                        obj.GetComponentInChildren<Text>().text = "确定";
                    },
                    (index, obj) =>
                    {
                        TipsController.instance.HideSelectUI2();
                        RoomController.instance.ChangeRoom(eRoomView.Bedroom1);
                        GameController.instance.ChangeState(eGameOperatorState.Sleep);
                    });
            }
        }

        public void ForceSetValue(IEffect effect)
        {
            infection = effect.changeInfection;
            emo = effect.changeEMO;
            health = effect.changeHealth;
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
        }

        private void OnTimeUpdate(EventParam.TimeUpdateParam param)
        {
            if (param.crossDay)
            {
                //10号发薪日  发工资
                if (GameController.instance.model.currentDay > nextPayMoneyDay)
                {
                    AudioManager.instance.PlayRewardSound();
                    TipsController.instance.ShowTips($"发工资啦!!!   金钱+{bankMoney}");
                    money += bankMoney;
                    bankMoney = 0;
                    EventManager.Get<EventDefine.PlayerValueChanged>().Fire();
                    nextPayMoneyDay = GetNextPayMoneyDay();
                }
            }
        }

        public void AddAbilityExp(eAbilityType type, int exp)
        {
            if (!abilityExp.ContainsKey(type))
            {
                abilityExp.Add(type, exp);
            }
            else
            {
                abilityExp[type] += exp;
            }
            int currentLevel = abilityLevel[type];
            while (true)
            {
                int upgradeNeedExp = ConfigManager.instance.playerConfig.GetUpgradeNeedExp(type, currentLevel);
                if (abilityExp[type] < upgradeNeedExp)
                {
                    break;
                }
                else
                {
                    abilityExp[type] -= upgradeNeedExp;
                    currentLevel++;
                }
            }
            if (abilityLevel[type] != currentLevel)
            {
                AudioManager.instance.PlayRewardSound();
                TipsController.instance.ShowTips($"{ConfigManager.instance.playerConfig.GetAbilityStringByType(type)} Lv.{abilityLevel[type]} => Lv.{currentLevel}");
                abilityLevel[type] = currentLevel;
                EventManager.Get<EventDefine.AbilityUpgrade>().Fire(type, abilityLevel[type]);
            }
        }

        public int GetNextPayMoneyDay()
        {
            int currentDay = GameController.instance.model.currentDay;
            while (true)
            {
                currentDay++;
                if (currentDay % 30 == 10)
                {
                    return currentDay;
                }
            }
        }

        public bool TryRollInfection()
        {
            return Random.value < infection * 0.33f;
        }
    }
}