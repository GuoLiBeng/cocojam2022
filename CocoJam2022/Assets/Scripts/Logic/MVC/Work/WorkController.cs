namespace Logic
{
    public class WorkController : SingletonClass<WorkController>, IController<WorkModel>
    {
        public WorkModel model => WorkModel.instance;
        public WorkUI workUI;
        public void Init()
        {
            model.Init();
            InitViews();
        }

        private void InitViews()
        {
            workUI = new WorkUI();
            workUI.Init();
        }
        public void Destroy()
        {
            model.Destroy();
            DestoryViews();
        }

        private void DestoryViews()
        {
            workUI.Destroy();
            workUI = null;
        }

        public void ShowWorkUI()
        {
            workUI.Show();
        }

        public void HideWorkUI()
        {
            workUI.Hide();
        }
    }
}