using UnityEngine;

namespace Logic
{
    public class WorkUI : BaseUI
    {
        protected override string prefabName => "WorkUI";

        protected override UISort sort => UISort.WorkUI;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "workBtn":
                    EventManager.Get<EventDefine.WorkFinished>().Fire(new EventParam.WorkFinishedParam()
                    {
                        success = true,
                    });
                    break;
            }

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }
    }
}