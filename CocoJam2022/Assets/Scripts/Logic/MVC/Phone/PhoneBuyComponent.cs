using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class PhoneBuyComponent : MonoBehaviour
    {
        public GameObject success;
        public GameObject fail;
        public GameObject loading;
        public bool loadingEnable
        {
            get => loading.gameObject.activeSelf;
            set
            {
                loading.gameObject.SetActiveOptimize(value);
            }
        }
        public CanvasGroup tips;
        private bool m_tipsEnable = false;
        public bool tipsEnable
        {
            get => m_tipsEnable;
            set
            {

                m_tipsEnable = value;
                if (value)
                {
                    tips.alpha = 1;
                    tipsTimeCounter = 0;
                }
                else
                {
                    if (tipsTimeCounter < tipsStayShowTime)
                    {
                        tipsTimeCounter = tipsStayShowTime;
                    }
                }

            }
        }
        private const float tipsStayShowTime = 0.3f;
        private const float tipsHideTime = 0.5f;
        private float tipsTimeCounter = 999;

        void OnEnable()
        {
            loadingEnable = false;
            tipsTimeCounter = 999;
            tips.alpha = 0;
            m_tipsEnable = false;
            success.gameObject.SetActiveOptimize(false);
            fail.gameObject.SetActiveOptimize(false);
        }

        private void Update()
        {
            if (tipsTimeCounter < tipsHideTime)
            {
                tipsTimeCounter += Time.deltaTime;
                if (tipsTimeCounter < tipsStayShowTime)
                {
                    tips.alpha = 1;
                }
                else if (tipsTimeCounter >= tipsHideTime)
                {
                    tips.alpha = 0;
                }
                else
                {
                    if (m_tipsEnable)
                        m_tipsEnable = false;
                    tips.alpha = (tipsHideTime - tipsTimeCounter) / (tipsHideTime - tipsStayShowTime);
                }
            }
        }
    }
}