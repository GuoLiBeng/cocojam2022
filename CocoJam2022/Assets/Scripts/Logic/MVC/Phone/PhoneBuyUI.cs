using UnityEngine;
using Config;

namespace Logic
{
    public class PhoneBuyUI : BaseUI
    {
        protected override string prefabName => "PhoneBuyUI";

        protected override UISort sort => UISort.PhoneBuyUI;
        private PhoneBuyComponent component;
        private bool isFinished = false;
        private bool result = false;

        private float buyTime = 0;

        private bool isLoading = false;
        private float loadingTimeCounter = 0;
        private float loadingTime = 0;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "exit":
                    if (isFinished)
                    {
                        EventManager.Get<EventDefine.MorningBugResult>().Fire(result);
                    }
                    break;
                case "BuyBtn":
                    OnBuyBtnClicked();
                    break;
            }
        }

        protected override void OnInit()
        {
            component = sceneObject.GetComponent<PhoneBuyComponent>();
        }

        protected override void OnShow()
        {
            isFinished = false;
            result = false;
            buyTime = Random.Range(ConfigManager.instance.buyConfig.minBuyTime, ConfigManager.instance.buyConfig.maxBuyTime);
            isLoading = false;
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }

        private void OnBuyBtnClicked()
        {
            if (!isLoading)
            {
                AudioManager.instance.PlayEffect("抢菜_点按屏幕");
                component.loadingEnable = true;
                component.tipsEnable = false;
                isLoading = true;
                loadingTimeCounter = 0;
                loadingTime = Random.Range(ConfigManager.instance.buyConfig.minLoadingTime, ConfigManager.instance.buyConfig.maxLoadingTime);
            }
        }

        public void Update()
        {
            if (isLoading)
            {
                loadingTimeCounter += Time.deltaTime;
                if (loadingTimeCounter >= loadingTime)
                {
                    TryBuyItem();
                }
            }
            if (buyTime > 0)
            {
                buyTime -= Time.deltaTime;
            }
        }

        private void TryBuyItem()
        {
            if (buyTime <= 0)
            {
                Finish(false);
                return;
            }
            if (Random.value < ConfigManager.instance.buyConfig.successRate)
            {
                Finish(true);
            }
            else
            {
                isLoading = false;
                component.loadingEnable = false;
                component.tipsEnable = true;
            }
        }
        private void Finish(bool success)
        {
            isLoading = false;
            component.loadingEnable = false;
            component.tipsEnable = false;

            result = success;
            isFinished = true;
            if (success)
            {
                RefrigeratorModel.instance.AddRandomItems();
                component.success.SetActiveOptimize(true);
            }
            else
            {
                component.fail.SetActiveOptimize(true);
            }
        }
    }
}