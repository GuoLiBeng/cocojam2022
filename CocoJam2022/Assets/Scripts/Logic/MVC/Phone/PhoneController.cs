using System.Collections.Generic;

namespace Logic
{
    public enum ePhoneUI
    {
        Buy,
    }
    public class PhoneController : SingletonClass<PhoneController>, IController<PhoneModel>
    {
        public PhoneModel model => PhoneModel.instance;

        public Dictionary<ePhoneUI, BaseUI> uiDict;

        public void Init()
        {
            model.Init();
            InitViews();
        }

        public void InitViews()
        {
            uiDict = new Dictionary<ePhoneUI, BaseUI>();
            uiDict.Add(ePhoneUI.Buy, new PhoneBuyUI());
            foreach (var item in uiDict)
            {
                item.Value.Init();
            }
        }

        public void Destroy()
        {

            model.Destroy();
        }

        public void DestroyViews()
        {
            foreach (var item in uiDict)
            {
                item.Value.Destroy();
            }
            uiDict = null;
        }

        public void ShowUI(ePhoneUI ui)
        {
            uiDict[ui].Show();
        }

        public void HideUI(ePhoneUI ui)
        {
            uiDict[ui].Hide();
        }

        public void Update()
        {
            (uiDict[ePhoneUI.Buy] as PhoneBuyUI).Update();
        }
    }
}