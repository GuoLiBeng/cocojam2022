using UnityEngine;
using Config;

namespace Logic
{
    public class FreeUI : BaseUI
    {

        protected override UISort sort => UISort.FreeUI;
        protected override string prefabName => "FreeUI";

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnFinish":
                    ModuleManager.instance.DestroyAllModules();
                    ModuleManager.instance.InitAllModules();
                    break;
            }
        }
    }
}