using UnityEngine;

namespace Logic
{
    public class GameModel : SingletonClass<GameModel>, IModel
    {
        public const int AllDayTime = 60 * 24;
        public int currentTime { get; private set; } = 0;  //分钟
        public int currentDay { get; private set; } = 0;
        public bool inFirstDay { get; private set; } = true;


        public int leftQuarantineDays = 0;
        private int currentLeftDaysIndex = 0;
        public float freeChance;
        public string leftDaysString => leftQuarantineDays < 0 ? "?" : leftQuarantineDays.ToString();
        public void Init()
        {
            leftQuarantineDays = Config.ConfigManager.instance.gameConfig.quarantineDays[currentLeftDaysIndex];
            currentLeftDaysIndex = 0;

            freeChance = Config.ConfigManager.instance.gameConfig.freeChance;

            currentTime = 0;
            currentDay = 0;
            inFirstDay = true;
        }
        public void Destroy()
        {

        }
        public void SpendTime(int time)
        {
            currentTime += time;
            bool crossDay = false;
            while (currentTime > AllDayTime)
            {
                currentDay++;
                crossDay = true;
                currentTime = currentTime - AllDayTime;
                OnCrossDay();
            }
            if (currentTime > 20 && currentTime < 24)
            {
                AudioManager.instance.PlayBGM("bgm_bedtime");
            }

            EventManager.Get<EventDefine.TimeUpdate>().Fire(new EventParam.TimeUpdateParam()
            {
                SpendTime = time,
                currentTime = currentTime,
                crossDay = crossDay,
            });
        }

        private void OnCrossDay()
        {
            //增加解封概率
            freeChance += Config.ConfigManager.instance.gameConfig.addFreeChance;

            //封锁日期减一
            if (leftQuarantineDays < 0)
            {
                return;
            }

            leftQuarantineDays--;

            if (leftQuarantineDays <= 0)
            {
                currentLeftDaysIndex++;
                if (currentLeftDaysIndex < Config.ConfigManager.instance.gameConfig.quarantineDays.Length)
                {
                    leftQuarantineDays = Config.ConfigManager.instance.gameConfig.quarantineDays[currentLeftDaysIndex];
                }
                else
                {
                    leftQuarantineDays = -1;
                }
                TipsController.instance.ShowTips($"解封日到了，可是收到通知又要继续封控{leftDaysString}天...");
            }
        }

        //强制推进到下一个这个时间
        public void ForceSetTime(int hour, int minute)
        {
            var oldTime = currentTime;
            currentTime = hour * 60 + minute;
            bool crossDay = currentTime < oldTime;
            int spendTime = currentTime - oldTime;
            if (crossDay)
            {
                currentDay++;
                spendTime += AllDayTime;
                OnCrossDay();
            }

            EventManager.Get<EventDefine.TimeUpdate>().Fire(new EventParam.TimeUpdateParam()
            {
                SpendTime = spendTime,
                currentTime = currentTime,
                crossDay = crossDay,
            });
        }

        public bool TryRollFree()
        {
            bool value = Random.value < freeChance;
            return value;
        }
    }
}