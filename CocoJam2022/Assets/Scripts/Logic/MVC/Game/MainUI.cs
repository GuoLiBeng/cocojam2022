using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class MainUI : BaseUI
    {
        protected override string prefabName => "MainUI";

        protected override UISort sort => UISort.MainUI;
        private Slider timeLineSlider;
        private Text timeLineTxt;
        private Slider infectionSlider;
        private Text infectionTxt;
        private Slider emoSlider;
        private Text emoTxt;
        private Slider healthSlider;
        private Text healthTxt;
        private Slider moneySlider;
        private Text moneyTxt;
        private Text currentDayTxt;
        protected override void OnInit()
        {
            //当前在家第几天
            currentDayTxt = GetChild<Text>("up/day/Text");
            // timeLineSlider = GetChild<Slider>("bottom/timeLine");
            // timeLineTxt = GetChild<Text>("bottom/timeLine/Text");
            infectionSlider = GetChild<Slider>("leftup/infection");
            infectionTxt = GetChild<Text>("leftup/infection/Text");
            emoSlider = GetChild<Slider>("leftup/emo");
            emoTxt = GetChild<Text>("leftup/emo/Text");
            healthSlider = GetChild<Slider>("leftup/health");
            healthTxt = GetChild<Text>("leftup/health/Text");
            moneySlider = GetChild<Slider>("leftup/money");
            moneyTxt = GetChild<Text>("leftup/money/Text");

            ClickableObj.Set(GetChild("up/mapBtn"), (GameObject obj) =>
            {
                AudioManager.instance.PlayEffect("UI_click");
                RoomController.instance.roomUI.Show();
            }, "打开地图");

            ClickableObj.Set(GetChild("up/timeBtn"), null, () => $"现在时间 {StaticUtils.MinuteToString(GameModel.instance.currentTime)}");
            ClickableObj.Set(GetChild("up/playerInfoBtn"), null, () =>
            {
                return $"下个发薪日:第{PlayerModel.instance.GetNextPayMoneyDay()}天\n做饭能力:{PlayerModel.instance.abilityLevel[eAbilityType.Cook]}\n整理能力:{PlayerModel.instance.abilityLevel[eAbilityType.Settle]}";
            });
            ClickableObj.Set(infectionSlider.gameObject, null, "受感染概率");
            ClickableObj.Set(emoSlider.gameObject, null, "SAN值");
            ClickableObj.Set(healthSlider.gameObject, null, "体能");
            ClickableObj.Set(moneySlider.gameObject, null, "金钱");
            ClickableObj.Set(GetChild("up/day"), null, () => $"距离解封还有 {GameModel.instance.leftDaysString} 天");
        }

        protected override void OnShow()
        {
            RefreshDay();
            // RefreshTimeLine();
            RefreshPlayerInfo();
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.TimeUpdate>().AddListener(OnTimeUpdate);
            EventManager.Get<EventDefine.PlayerValueChanged>().AddListener(OnPlayerValueChanged);
        }
        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.TimeUpdate>().RemoveListener(OnTimeUpdate);
            EventManager.Get<EventDefine.PlayerValueChanged>().RemoveListener(OnPlayerValueChanged);
        }
        protected override void OnClick(GameObject obj)
        {

        }

        private void OnTimeUpdate(object p)
        {
            var param = p as EventParam.TimeUpdateParam;
            //RefreshTimeLine();
            if (param.crossDay)
            {
                RefreshDay();
            }
        }

        private void OnPlayerValueChanged()
        {
            RefreshPlayerInfo();
        }

        private void RefreshPlayerInfo()
        {
            var infection = PlayerController.instance.model.infection;
            infectionSlider.value = infection;
            infectionTxt.text = (infection * 100).ToString();

            emoTxt.text = (ConfigManager.instance.playerConfig.maxEmo - PlayerController.instance.model.emo).ToString();
            emoSlider.value = 1 - (float)PlayerController.instance.model.emo / ConfigManager.instance.playerConfig.maxEmo;

            healthTxt.text = PlayerController.instance.model.health.ToString();
            healthSlider.value = (float)PlayerController.instance.model.health / ConfigManager.instance.playerConfig.maxHealth;

            moneyTxt.text = PlayerController.instance.model.money.ToString();
            moneySlider.value = 1;
        }

        private void RefreshTimeLine()
        {
            var currentTime = GameController.instance.model.currentTime;
            timeLineSlider.value = (float)currentTime / (float)GameModel.AllDayTime;
            timeLineTxt.text = StaticUtils.MinuteToString(currentTime);
        }

        private void RefreshDay()
        {
            currentDayTxt.text = GameController.instance.model.currentDay.ToString();
        }
    }
}