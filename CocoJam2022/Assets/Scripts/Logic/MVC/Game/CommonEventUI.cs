using UnityEngine;
using Config;

namespace Logic
{
    public class CommonEventUI : BaseUI
    {

        protected override UISort sort => UISort.EmergencyUI;
        protected override string prefabName => "CommonEventUI";
        private CommonEventUIComponent component;
        private EventItemConfig config;

        protected override void OnInit()
        {
            component = sceneObject.GetComponent<CommonEventUIComponent>();
        }

        public void Show(EventItemConfig config)
        {
            this.config = config;
            base.Show();

        }

        protected override void OnShow()
        {
            component.Init(config);
            component.StartPlay();
        }

        protected override void OnHide()
        {
            component.StopPlay();
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}