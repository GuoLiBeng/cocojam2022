using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    public class GameController : SingletonClass<GameController>, IController<GameModel>
    {
        public GameModel model => GameModel.instance;
        public MainUI mainUI;
        private IntroUI introUI;
        public CommonEventUI commonEventUI;
        public GameUI gameUI;
        public FreeUI freeUI;
        public OperateMachine<eGameOperatorState> operateMachine;

        public eGameOperatorState currentState => operateMachine.currentState;
        public void Init()
        {
            model.Init();
            InitView();
            RegisterEvents();
            InitOperateMachine();
        }

        private void InitView()
        {
            mainUI = new MainUI();
            mainUI.Init();
            introUI = new IntroUI();
            introUI.Init();
            commonEventUI = new CommonEventUI();
            commonEventUI.Init();
            freeUI = new FreeUI();
            freeUI.Init();
        }

        private void InitOperateMachine()
        {
            operateMachine = new OperateMachine<eGameOperatorState>();
            var states = new Dictionary<eGameOperatorState, BaseOperator<eGameOperatorState>>();
            states.Add(eGameOperatorState.Empty, new GameOperatorEmpty(operateMachine));
            states.Add(eGameOperatorState.FirstEnter, new GameOperatorFirstEnter(operateMachine));
            states.Add(eGameOperatorState.Buy, new GameOperatorBuy(operateMachine));
            states.Add(eGameOperatorState.Work, new GameOperatorWork(operateMachine));
            states.Add(eGameOperatorState.Cook, new GameOperatorCook(operateMachine));
            states.Add(eGameOperatorState.SettleRes, new GameOperatorSettleRes(operateMachine));
            states.Add(eGameOperatorState.Sleep, new GameOperatorSleep(operateMachine));
            states.Add(eGameOperatorState.Rest, new GameOperatorRest(operateMachine));
            states.Add(eGameOperatorState.Play, new GameOperatorPlay(operateMachine));
            states.Add(eGameOperatorState.Quarantine, new GameOperatorQuarantine(operateMachine));
            states.Add(eGameOperatorState.Free, new GameOperatorFree(operateMachine));
            operateMachine.Init(states, eGameOperatorState.FirstEnter);
        }

        private void RegisterEvents()
        {
            //EventManager.AddListener(eEvent.ModulesInitFinished, OnModulesInitFinished);
            EventManager.Get<EventDefine.ModulesInitFinished>().AddListener(OnModulesInitFinished);
        }

        private void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().RemoveListener(OnModulesInitFinished);
        }

        public void Destroy()
        {
            operateMachine.Destroy();
            operateMachine = null;
            UnRegisterEvents();
            mainUI.Destroy();
            mainUI = null;
            model.Destroy();
            introUI.Destroy();
            introUI = null;
            commonEventUI.Destroy();
            commonEventUI = null;
            freeUI.Destroy();
            freeUI = null;
        }

        public void Update()
        {
            operateMachine.Update();
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (gameUI == null)
                {
                    gameUI = new GameUI();
                    gameUI.Init();
                }
                if (gameUI.isShow)
                {
                    gameUI.Hide();
                }
                else
                {
                    gameUI.Show();
                }

            }
        }

        private void OnModulesInitFinished()
        {
            mainUI.Show();
            //operateMachine.ChangeState(eGameOperatorState.FirstEnter);
        }

        public void ChangeState(eGameOperatorState state)
        {
            operateMachine.ChangeState(state);
        }

        public void ChangeState<T>(eGameOperatorState state, T param)
        {
            operateMachine.ChangeState<T>(state, param);
        }

        public void ShowIntroUI()
        {
            introUI.Show();
        }

        public void HideIntroUI()
        {
            introUI.Hide();
        }
    }
}