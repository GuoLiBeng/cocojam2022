using UnityEngine;
using UnityEngine.UI;
using Config;
using System.Collections;
using System.Collections.Generic;

namespace Logic
{
    public class CommonEventUIComponent : MonoBehaviour
    {
        public Text tipsTxt;
        public Text descTxt;
        public Slider slider;
        public Text sliderTxt;
        public Button continueBtn;
        public Button stopBtn;
        public Text continueBtnTxt;
        public Text stopBtnTxt;

        public EventItemConfig eventConfig;

        private const float EventTime = 5.0f;
        //被打断后恢复做事效率倍数
        private const float continueScale = 0.5f;

        private float currentPercent = 0;
        private EventItemConfig emergencyConfig;
        private float emergencyStartPercent;
        private bool haveEmergency => emergencyConfig != null;

        private Coroutine eventDescCoroutine;
        private Coroutine emergencyCoroutine;
        private bool isContinue;
        public void Awake()
        {
            continueBtn.onClick.AddListener(OnContinueBtnClicked);
            stopBtn.onClick.AddListener(OnStopBtnClicked);
        }

        //初始化本次数据
        public void Init(EventItemConfig config)
        {
            eventConfig = config;
            currentPercent = 0;
            isContinue = true;
            emergencyConfig = TryCreateEmergency();
            if (emergencyConfig != null)
            {
                emergencyStartPercent = Random.value;
            }
            continueBtn.gameObject.SetActiveOptimize(false);
            stopBtn.gameObject.SetActiveOptimize(false);
            continueBtnTxt.text = "继续" + config.eventName;
            stopBtnTxt.text = "停止" + config.eventName;

            tipsTxt.transform.parent.gameObject.SetActiveOptimize(false);
            descTxt.transform.parent.gameObject.SetActiveOptimize(false);
        }

        public void StartPlay()
        {
            StartCoroutine(I_Start());
        }

        public void StopPlay()
        {
            StopAllCoroutines();
        }

        IEnumerator I_Start()
        {
            if (haveEmergency)
            {
                ShowTips(eventConfig.desc);
                yield return I_AddPercent(emergencyStartPercent);
                HideTips();
                yield return I_ShowEmergency();
            }
            else
            {
                ShowTips(eventConfig.desc);
                yield return I_AddPercent(1);
                Finish();
            }
        }

        IEnumerator I_AddPercent(float target)
        {
            while (currentPercent < target)
            {
                currentPercent += Time.deltaTime / EventTime;
                SetSliderValue(currentPercent);
                yield return null;
            }
        }

        IEnumerator I_ShowEmergency()
        {
            if (emergencyConfig.eventName == "突发事件-核酸检查"
            || emergencyConfig.eventName == "突发事件-核酸检查")
            {
                AudioManager.instance.PlayBGM("bgm_核酸还是抗原_lp");
            }
            else if (emergencyConfig.eventName == "突发事件-猫咪闯祸")
            {
                AudioManager.instance.PlayBGM("bgm_宠物偷肉_lp");
            }
            yield return I_EmergencyDesc(emergencyConfig.desc);
            continueBtn.gameObject.SetActiveOptimize(true);
            stopBtn.gameObject.SetActiveOptimize(true);
        }

        IEnumerator I_ShowEventDesc(string desc)
        {
            int dotCount = 1;
            while (true)
            {
                string txt = desc;
                for (int i = 0; i < dotCount; i++)
                {
                    txt += ".";
                }
                tipsTxt.text = txt;
                yield return new WaitForSeconds(0.3f);
                dotCount++;
                if (dotCount >= 7)
                {
                    dotCount = 1;
                }
            }
        }

        IEnumerator I_EmergencyDesc(string desc)
        {
            descTxt.transform.parent.gameObject.SetActiveOptimize(true);
            int charactorAmount = desc.Length;
            string showTxt = emergencyConfig.eventName + "\n";
            for (int i = 0; i < charactorAmount; i++)
            {
                showTxt += desc[i];
                descTxt.text = showTxt;
                yield return new WaitForSeconds(0.1f);
            }
        }

        private void OnContinueBtnClicked()
        {
            EmergencyFinish();
            isContinue = true;
            ShowTips(eventConfig.desc);
            StartCoroutine(I_OnContinueBtnClicked());
        }

        IEnumerator I_OnContinueBtnClicked()
        {
            yield return I_AddPercent(1);
            Finish();
        }

        private void OnStopBtnClicked()
        {
            EmergencyFinish();
            isContinue = false;
            Finish();
        }

        private void EmergencyFinish()
        {
            AudioManager.instance.StopBGM();
            continueBtn.gameObject.SetActiveOptimize(false);
            stopBtn.gameObject.SetActiveOptimize(false);
            descTxt.transform.parent.gameObject.SetActiveOptimize(false);
            GameModel.instance.SpendTime(emergencyConfig.costTime);
            PlayerModel.instance.ApplyEffect(emergencyConfig);


            Dictionary<string, int> itemDict = new Dictionary<string, int>();
            foreach (var item in emergencyConfig.addItemList)
            {
                RefrigeratorModel.instance.AddNewOutSideItem(item.itemName, item.amount);
                if (itemDict.ContainsKey(item.itemName))
                {
                    itemDict[item.itemName] += item.amount;
                }
                else
                {
                    itemDict.Add(item.itemName, item.amount);
                }
            }

            if (emergencyConfig.eventName == "突发事件-分发物资"
            || emergencyConfig.eventName == "突发事件-社区团购"
            || emergencyConfig.eventName == "突发事件-公司物资")
            {
                int typeAmount = Random.Range(ConfigManager.instance.buyConfig.minItemTypeAmount, ConfigManager.instance.buyConfig.maxItemTypeAmount);
                for (int i = 0; i < typeAmount; i++)
                {
                    string itemName = ConfigManager.instance.itemConfig.items[Random.Range(0, ConfigManager.instance.itemConfig.items.Length)].itemName;
                    int amount = Random.Range(ConfigManager.instance.buyConfig.minItemAmount, ConfigManager.instance.buyConfig.maxItemAmount);
                    RefrigeratorModel.instance.AddNewOutSideItem(itemName, amount);
                    if (itemDict.ContainsKey(itemName))
                    {
                        itemDict[itemName] += amount;
                    }
                    else
                    {
                        itemDict.Add(itemName, amount);
                    }
                }
            }

            if (itemDict.Count > 0)
            {
                string log = "获得物资  ";
                int count = 0;
                foreach (var pairs in itemDict)
                {
                    count++;
                    log += $"{pairs.Key}×{pairs.Value}";
                    if (count < itemDict.Count)
                    {
                        log += "、";
                    }
                }
                AudioManager.instance.PlayRewardSound();
                TipsController.instance.ShowTips(log);
            }

        }

        private void Finish()
        {
            StopPlay();
            float valueScale = 1.0f;
            if (haveEmergency)
            {
                if (isContinue)
                {
                    valueScale = emergencyStartPercent + (1 - emergencyStartPercent) * continueScale;
                }
                else
                {
                    valueScale = emergencyStartPercent;
                }
            }
            EventManager.Get<EventDefine.CommonEventUIResult>().Fire(new EventParam.CommonEventUIResultParam()
            {
                haveEmergency = haveEmergency,
                emergencyConfig = emergencyConfig,
                completePercent = isContinue ? 1.0f : currentPercent,
                isContinue = isContinue,
                valueScale = valueScale,
            });
        }

        private EventItemConfig TryCreateEmergency()
        {
            //不能被打断就返回
            if (!eventConfig.CanBeInterruptedByType(eEventType.Emergency)) return null;

            if (Random.value < eventConfig.emergencyProbability)
            {
                return ConfigManager.instance.eventConfig.GetRandomEmergency(eventConfig);
            }
            return null;
        }

        public void SetSliderValue(float value)
        {
            sliderTxt.text = string.Format("{0}%", (int)(value * 100));
            slider.value = value;
        }

        private void ShowTips(string tips)
        {
            tipsTxt.transform.parent.gameObject.SetActiveOptimize(true);
            eventDescCoroutine = StartCoroutine(I_ShowEventDesc(eventConfig.desc));
        }

        private void HideTips()
        {
            tipsTxt.transform.parent.gameObject.SetActiveOptimize(false);
            StopCoroutine(eventDescCoroutine);
        }
    }
}