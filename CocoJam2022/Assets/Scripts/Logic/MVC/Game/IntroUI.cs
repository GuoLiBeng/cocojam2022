using UnityEngine;

namespace Logic
{
    public class IntroUI : BaseUI
    {
        protected override UISort sort => UISort.IntroUI;

        protected override string prefabName => "IntroUI";

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnStart":
                    AudioManager.instance.PlayEffect("UI_click");
                    GameController.instance.ChangeState(eGameOperatorState.Buy);
                    //GameController.instance.ChangeState(eGameOperatorState.Empty);
                    break;
            }
        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
    }
}