using Config;
using UnityEngine;
using System.Collections.Generic;

namespace Logic
{
    public class SleepController : SingletonClass<SleepController>, IController<SleepModel>
    {
        public SleepModel model => SleepModel.instance;

        public void Destroy()
        {

        }

        public void Init()
        {

        }
    }
}