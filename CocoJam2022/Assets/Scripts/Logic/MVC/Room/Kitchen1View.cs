using UnityEngine;

namespace Logic
{
    public class Kitchen1View : BaseObject
    {
        protected override string prefabName => "Kitchen1";

        protected override void OnClick(GameObject obj)
        {
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            ClickableObj.Set(GetChild("clickToBedroom2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom2);
            },
            "去卧室2");
            ClickableObj.Set(GetChild("clickToKitchen2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Kitchen2);
            },
            "去厨房2");
            ClickableObj.Set(GetChild("clickToCookStove"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.CookStove);
            },
            "去灶台", false);
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("KitchenRoomTone_lp");
        }
    }
}