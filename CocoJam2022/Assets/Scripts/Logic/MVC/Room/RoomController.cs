using System.Collections.Generic;

namespace Logic
{
    public enum eRoomView
    {
        Bedroom1,
        Bedroom2,
        Kitchen1,
        Kitchen2,
        Work,
        CookStove,
        CookSink,
        Balcony,
    }
    public class RoomController : SingletonClass<RoomController>, IController<RoomModel>
    {
        public RoomModel model => RoomModel.instance;

        public Dictionary<eRoomView, BaseObject> viewDict;
        public RoomUI roomUI;

        private eRoomView currentRoom = eRoomView.Bedroom1; //默认初始是从卧室开始的

        public void Init()
        {
            model.Init();
            InitViews();
        }

        public void InitViews()
        {
            viewDict = new Dictionary<eRoomView, BaseObject>();
            viewDict.Add(eRoomView.Bedroom1, new Bedroom1View());
            viewDict.Add(eRoomView.Bedroom2, new Bedroom2View());
            viewDict.Add(eRoomView.Kitchen1, new Kitchen1View());
            viewDict.Add(eRoomView.Kitchen2, new Kitchen2View());
            viewDict.Add(eRoomView.Work, new WorkView());
            viewDict.Add(eRoomView.CookSink, new CookSinkView());
            viewDict.Add(eRoomView.CookStove, new CookStoveView());
            viewDict.Add(eRoomView.Balcony, new BalconyView());
            foreach (var view in viewDict.Values)
            {
                view.Init();
            }
            viewDict[currentRoom].Show();

            roomUI = new RoomUI();
            roomUI.Init();
        }

        public void ChangeRoom(eRoomView newRoom)
        {
            if (newRoom != currentRoom)
            {
                AudioManager.instance.PlayEffect("UI_fs_slippers");
                viewDict[currentRoom].Hide();
                currentRoom = newRoom;
                viewDict[currentRoom].Show();
            }
        }

        public void Destroy()
        {
            DestroyViews();
            model.Destroy();
        }

        public void DestroyViews()
        {
            foreach (var view in viewDict.Values)
            {
                view.Destroy();
            }
            viewDict = null;

            roomUI.Destroy();
            roomUI = null;
        }

        public void Update()
        {
            if (viewDict[eRoomView.Work].isShow) (viewDict[eRoomView.Work] as WorkView).Update();
        }
    }
}