using UnityEngine;
using Config;
namespace Logic
{
    public class BalconyView : BaseObject
    {
        protected override string prefabName => "Balcony";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            ClickableObj.Set(GetChild("clickToBedroom1"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom1);
            },
            "去卧室1");
            ClickableObj.Set(GetChild("clickToBedroom2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom2);
            },
            "去卧室2");
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("KitchenRoomTone_lp");
        }
    }
}