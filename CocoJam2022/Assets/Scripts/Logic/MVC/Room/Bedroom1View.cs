using UnityEngine;
using Config;
using UnityEngine.UI;

namespace Logic
{
    public class Bedroom1View : BaseObject
    {
        protected override string prefabName => "Bedroom1";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            ClickableObj.Set(GetChild("clickToBedroom2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom2);
            },
            "去卧室2");
            ClickableObj.Set(GetChild("clickToBalcony"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Balcony);
            },
            "去阳台");
            ClickableObj.Set(GetChild("床"),
            (obj) =>
            {
                GameController.instance.ChangeState(eGameOperatorState.Sleep);
            },
            "一觉睡到早上6点");
            ClickableObj.Set(GetChild("clickToRest"),
            (obj) =>
            {
                TipsController.instance.OpenSelectUI(2, (index, obj) =>
                {
                    if (index == 0)
                    {
                        obj.GetComponentInChildren<Text>().text = "躺平";
                        ClickableObj.Set(obj,
                            (obj) =>
                            {
                                TipsController.instance.HideSelectUI();
                                GameController.instance.ChangeState<EventItemConfig>(eGameOperatorState.Rest,
                                ConfigManager.instance.eventConfig.GetConfigByName("躺平"));
                            },
                            ConfigManager.instance.eventConfig.GetConfigByName("躺平"));
                    }
                    else if (index == 1)
                    {
                        obj.GetComponentInChildren<Text>().text = "玩手机";
                        ClickableObj.Set(obj,
                            (obj) =>
                            {
                                TipsController.instance.HideSelectUI();
                                GameController.instance.ChangeState<EventItemConfig>(eGameOperatorState.Rest,
                                ConfigManager.instance.eventConfig.GetConfigByName("玩手机"));
                            },
                            ConfigManager.instance.eventConfig.GetConfigByName("玩手机"));
                    }
                });
            },
            "去沙发边上");
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("BedRoomTone_lp");
        }

        protected override void OnEnter(GameObject obj)
        {

        }

        protected override void OnExit(GameObject obj)
        {

        }
    }
}