using UnityEngine;
using Config;
using UnityEngine.UI;

namespace Logic
{
    public class WorkView : BaseObject
    {
        protected override string prefabName => "Work";
        private GameObject computer;
        private EventItemConfig config;
        private bool isAboveCoca = false;
        protected override void OnInit()
        {
            config = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Work);
            ClickableObj.Set(GetChild("电脑"),
            (obj) =>
            {
                TipsController.instance.OpenSelectUI(2, (index, obj) =>
                {
                    if (index == 0)
                    {
                        obj.GetComponentInChildren<Text>().text = "工作";
                        ClickableObj.Set(obj,
                                    (obj) =>
                                    {
                                        TipsController.instance.HideSelectUI();
                                        GameController.instance.ChangeState(eGameOperatorState.Work);
                                    }, ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Work));
                    }
                    else if (index == 1)
                    {
                        obj.GetComponentInChildren<Text>().text = "娱乐";
                        ClickableObj.Set(obj,
                                            (obj) =>
                                            {
                                                TipsController.instance.HideSelectUI();
                                                GameController.instance.ChangeState(eGameOperatorState.Play);
                                            }, ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Play));
                    }
                });
            },
            "打开电脑");
            ClickableObj.Set(GetChild("clickToBedroom2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom2);
            },
            "去卧室2");

            ClickableObj.Set(GetChild("可乐"),
            null,
            "按E直接喝掉,冰镇效果更佳哦~");
        }

        protected override void OnShow()
        {
            isAboveCoca = false;
            AudioManager.instance.PlayAGM("BedRoomTone_lp");
            GetChild("可乐").SetActiveOptimize(RefrigeratorModel.instance.HaveOutSideCoca());
        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnDestroy()
        {
        }
        protected override void OnClick(GameObject obj)
        {


        }

        protected override void OnEnter(GameObject obj)
        {
            if (obj.name == "可乐")
            {
                isAboveCoca = true;
            }
        }

        protected override void OnExit(GameObject obj)
        {
            if (obj.name == "可乐")
            {
                isAboveCoca = false;
            }
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.E) && isAboveCoca)
            {
                GetChild("可乐").SetActiveOptimize(false);
                RefrigeratorModel.instance.ConsumeOutSideCoca();
            }
        }
    }
}