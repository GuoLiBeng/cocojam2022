using Config;
using UnityEngine;

namespace Logic
{
    public class CookStoveView : BaseObject
    {
        protected override string prefabName => "CookStove";
        private EventItemConfig config;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            config = ConfigManager.instance.eventConfig.GetConfigByName("做饭");
            ClickableObj.Set(GetChild("clickToKitchen1"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Kitchen1);
            },
            "去厨房1");
            ClickableObj.Set(GetChild("Cook"),
            (obj) =>
            {
                CookController.instance.ShowCookUI();
            },
            "去做饭");
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("KitchenRoomTone_lp");
        }

        protected override void OnEnter(GameObject obj)
        {

        }

        protected override void OnExit(GameObject obj)
        {

        }
    }
}