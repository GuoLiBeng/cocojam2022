using UnityEngine;

namespace Logic
{
    public class Bedroom2View : BaseObject
    {
        protected override string prefabName => "Bedroom2";

        protected override void OnClick(GameObject obj)
        {
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            ClickableObj.Set(GetChild("电脑"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Work);
            },
            "去书桌");
            ClickableObj.Set(GetChild("clickToBalcony"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Balcony);
            },
            "去阳台");
            ClickableObj.Set(GetChild("clickToBedroom1"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Bedroom1);
            },
            "去卧室1");
            ClickableObj.Set(GetChild("clickToKitchen1"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Kitchen1);
            },
            "去厨房1");
            ClickableObj.Set(GetChild("Radio"),
            (obj) =>
            {
                AudioManager.instance.radioEnable = !AudioManager.instance.radioEnable;
            },
            "Radio");
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("BedRoomTone_lp");
        }

        protected override void OnEnter(GameObject obj)
        {

        }

        protected override void OnExit(GameObject obj)
        {

        }
    }
}