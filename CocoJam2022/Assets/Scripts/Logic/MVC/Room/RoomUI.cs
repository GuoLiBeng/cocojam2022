using UnityEngine;

namespace Logic
{
    public class RoomUI : BaseUI
    {
        protected override string prefabName => "RoomUI";

        protected override UISort sort => UISort.RoomUI;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "bedroom1":
                    OnRoomBtnClicked(eRoomView.Bedroom1);
                    break;
                case "bedroom2":
                    OnRoomBtnClicked(eRoomView.Bedroom2);
                    break;
                case "kitchen1":
                    OnRoomBtnClicked(eRoomView.Kitchen1);
                    break;
                case "kitchen2":
                    OnRoomBtnClicked(eRoomView.Kitchen2);
                    break;
                case "yangtai":
                    OnRoomBtnClicked(eRoomView.Balcony);
                    break;
                case "exit":
                    Hide();
                    break;
            }
        }

        private void OnRoomBtnClicked(eRoomView view)
        {
            AudioManager.instance.PlayEffect("UI_choose");
            RoomController.instance.ChangeRoom(view);
            Hide();
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }
    }
}