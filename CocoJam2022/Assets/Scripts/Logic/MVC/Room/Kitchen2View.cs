using UnityEngine;
using Config;
namespace Logic
{
    public class Kitchen2View : BaseObject
    {
        protected override string prefabName => "Kitchen2";

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {

            }
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            ClickableObj.Set(GetChild("clickToKitchen1"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Kitchen1);
            },
            "去厨房1");
            // ClickableObj.Set(GetChild("clickToCookSink"),
            // (obj) =>
            // {
            //     RoomController.instance.ChangeRoom(eRoomView.CookSink);
            // },
            // "去洗手池", false);
            var settleResConfig = ConfigManager.instance.eventConfig.GetConfigByType(eEventType.SettleRes);
            ClickableObj.Set(GetChild("clickRefrigerator"),
            (obj) =>
            {
                GameController.instance.ChangeState(eGameOperatorState.SettleRes);
            },
            settleResConfig);
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("KitchenRoomTone_lp");
        }
    }
}