using UnityEngine;
using Config;

namespace Logic
{
    public class CookSinkView : BaseObject
    {
        protected override string prefabName => "CookSink";
        private EventItemConfig config;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
        }

        protected override void OnInit()
        {
            config = ConfigManager.instance.eventConfig.GetConfigByName("洗菜");
            ClickableObj.Set(GetChild("clickToKitchen2"),
            (obj) =>
            {
                RoomController.instance.ChangeRoom(eRoomView.Kitchen2);
            },
            "去厨房2");
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("KitchenRoomTone_lp");
        }

        protected override void OnEnter(GameObject obj)
        {

        }

        protected override void OnExit(GameObject obj)
        {

        }
    }
}