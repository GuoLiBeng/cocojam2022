using UnityEngine;

namespace Logic
{
    public class DebugController : SingletonClass<DebugController>, IController<DebugModel>
    {
        public DebugModel model => DebugModel.instance;
        public DebugController()
        {
        }
        public void Init()
        {
            model.Init();
        }
        public void Destroy()
        {
            model.Destroy();
        }
        private int index = 0;
        public void Update()
        {
            // if (Input.GetKeyDown(KeyCode.F1))
            // {
            //     Application.OpenURL("https://www.bilibili.com");
            // }
            // if (Input.GetKeyDown(KeyCode.F2))
            // {
            //     GameController.instance.ChangeState(eGameOperatorState.Buy);
            // }
            // if (Input.GetKeyDown(KeyCode.P))
            // {
            //     UnityEditor.EditorApplication.isPaused = true;
            // }
            // if (Input.GetKeyDown(KeyCode.F))
            // {
            //     TipsController.instance.ShowTips(index++.ToString());
            // }
        }
    }
}