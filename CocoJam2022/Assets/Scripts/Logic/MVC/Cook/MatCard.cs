using UnityEngine;
using UnityEngine.UI;
using Config;
namespace Logic
{
    public class MatCard : MonoBehaviour
    {
        public Image icon;
        public Text amountTxt;
        public ItemConfigItem itemConfig;
        public FoodMaterial foodMatConfig;
        public bool enough;
        public void SetData(FoodMaterial config)
        {
            foodMatConfig = config;
            itemConfig = ConfigManager.instance.itemConfig.GetItemConfigByName(config.itemName);
            icon.sprite = itemConfig.icon;
            amountTxt.text = $"×{config.amount}";
            enough = RefrigeratorModel.instance.GetItemAmount(config.itemName) >= config.amount;
            amountTxt.color = enough ? new Color32(50, 50, 50, 255) : new Color32(130, 51, 41, 255);
        }
    }
}