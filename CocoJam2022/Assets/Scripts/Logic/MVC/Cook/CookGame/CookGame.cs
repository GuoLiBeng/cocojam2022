using UnityEngine;
using Config;

namespace Logic
{
    public class CookGame : BaseObject
    {
        protected override string prefabName => "CookGame";
        public override Vector3 initPos => new Vector3(0, 0, -50);
        public CookGameComponent component;
        protected override void OnInit()
        {
            component = sceneObject.GetComponent<CookGameComponent>();
        }

        protected override void OnShow()
        {
            component.Init();
        }
        protected override void OnHide()
        {
            component.Destroy();
        }
        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}