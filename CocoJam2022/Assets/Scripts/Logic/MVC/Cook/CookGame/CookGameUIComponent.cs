using UnityEngine;
using UnityEngine.UI;
using Config;
using System.Collections;

namespace Logic
{
    public class CookGameUIComponent : MonoBehaviour
    {
        public Text descTxt;
        public Button continueBtn;
        public Button stopBtn;
        public Text continueBtnTxt;
        public Text stopBtnTxt;

        public GameObject comboObject;
        public Text comboTxt;
        public GameObject resultPanel;
        public Text maxComboTxt;
        public Text perfectTxt;
        public Text goodTxt;
        public Text badTxt;
        public Text missTxt;
        public Text scaleTxt;

        private EventItemConfig emergencyConfig;
        //初始化本次数据
        public void Init(EventItemConfig config)
        {
            continueBtn.gameObject.SetActiveOptimize(false);
            stopBtn.gameObject.SetActiveOptimize(false);
            continueBtnTxt.text = "继续" + config.eventName;
            stopBtnTxt.text = "停止" + config.eventName;

            descTxt.transform.parent.gameObject.SetActiveOptimize(false);
        }

        public void PlayEmergency(EventItemConfig emergencyConfig)
        {
            this.emergencyConfig = emergencyConfig;
            StopAllCoroutines();
            StartCoroutine(I_ShowEmergency());
        }

        IEnumerator I_ShowEmergency()
        {
            yield return I_EmergencyDesc(emergencyConfig.desc);
            continueBtn.gameObject.SetActiveOptimize(true);
            stopBtn.gameObject.SetActiveOptimize(true);
        }

        IEnumerator I_EmergencyDesc(string desc)
        {
            descTxt.transform.parent.gameObject.SetActiveOptimize(true);
            int charactorAmount = desc.Length;
            string showTxt = emergencyConfig.eventName + "\n";
            for (int i = 0; i < charactorAmount; i++)
            {
                showTxt += desc[i];
                descTxt.text = showTxt;
                yield return new WaitForSeconds(0.1f);
            }
        }

        public void EmergencyFinish()
        {
            StopAllCoroutines();
            continueBtn.gameObject.SetActiveOptimize(false);
            stopBtn.gameObject.SetActiveOptimize(false);
            descTxt.transform.parent.gameObject.SetActiveOptimize(false);
        }

        public void ShowResult(int maxCombo, int allCount, int perfect, int good, int bad, int miss, float scale)
        {
            resultPanel.SetActiveOptimize(true);
            maxComboTxt.text = maxCombo == allCount ? $"最高连击数:{maxCombo}   All Combo!!!" : $"最高连击数:{maxCombo}";
            perfectTxt.text = $"Perfect:{perfect}";
            goodTxt.text = $"Good:{good}";
            badTxt.text = $"Bad:{bad}";
            missTxt.text = $"Miss:{miss}";
            scaleTxt.text = $"效果加成倍数:{scale}";
        }

        public void HideResult()
        {
            resultPanel.SetActiveOptimize(false);
        }
    }
}