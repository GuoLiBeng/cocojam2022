using UnityEngine;
using UnityEngine.UI;
using Config;
using System.Collections.Generic;

namespace Logic
{
    public class CookGameUI : BaseUI
    {
        protected override string prefabName => "CookGameUI";

        protected override UISort sort => UISort.CookUI;
        private EventItemConfig config => ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Cook);
        private CookGameUIComponent component;
        private int maxCombo = 0;
        private int comboIndex = 0;
        private int m_comboCount = -1;
        private int comboCount
        {
            get => m_comboCount;
            set
            {
                m_comboCount = value;
                if (m_comboCount > maxCombo)
                {
                    maxCombo = m_comboCount;
                }
                if (m_comboCount <= 1)
                {
                    component.comboObject.SetActiveOptimize(false);
                }
                else
                {
                    component.comboObject.SetActiveOptimize(true);
                    component.comboTxt.text = $"连击 {m_comboCount}";
                }
            }
        }
        private float scaleSum;
        private int scaleAmount;
        private Dictionary<EventParam.BeatResultType, int> beatResultDict = new Dictionary<EventParam.BeatResultType, int>();

        //被打断后恢复做事效率倍数
        private const float continueScale = 0.5f;
        private EventItemConfig emergencyConfig;
        private int emergencyStartIndex;
        private bool haveEmergency => emergencyConfig != null;
        private bool isContinue;
        private bool isTriggerEmergency = false;

        private EventParam.CookFinishedParam param = new EventParam.CookFinishedParam();

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "exit":
                    Hide();
                    break;
                case "continueBtn":
                    OnContinueBtnClicked();
                    break;
                case "stopBtn":
                    OnStopBtnClicked();
                    break;
                case "finishBtn":
                    EventManager.Get<EventDefine.CookFinished>().Fire(param);
                    break;
            }
        }

        protected override void OnInit()
        {
            component = sceneObject.GetComponent<CookGameUIComponent>();
            component.Init(config);
        }

        protected override void OnShow()
        {
            maxCombo = 0;
            comboCount = 0;
            comboIndex = -1;
            scaleSum = 0;
            scaleAmount = 0;
            beatResultDict.Clear();
            beatResultDict.Add(EventParam.BeatResultType.Perfect, 0);
            beatResultDict.Add(EventParam.BeatResultType.Good, 0);
            beatResultDict.Add(EventParam.BeatResultType.Bad, 0);
            beatResultDict.Add(EventParam.BeatResultType.Miss, 0);

            component.HideResult();
            CookController.instance.OpenCookGame();
            isTriggerEmergency = false;
            emergencyConfig = TryCreateEmergency();
            if (emergencyConfig != null)
            {
                emergencyStartIndex = Random.Range(0, CookController.instance.cookGame.component.clickCount - 1);
                Debug.LogError("emergencyStartIndex " + emergencyStartIndex);
            }
        }
        protected override void OnHide()
        {
            CookController.instance.HideCookGame();
        }

        protected override void OnDestroy()
        {

        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.BeatItemResult>().AddListener(OnGetBeatItemResult);
            EventManager.Get<EventDefine.CookGameResult>().AddListener(OnGetCookGameResult);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.BeatItemResult>().RemoveListener(OnGetBeatItemResult);
            EventManager.Get<EventDefine.CookGameResult>().RemoveListener(OnGetCookGameResult);
        }

        private void OnGetBeatItemResult(EventParam.BeatItemParam param)
        {
            beatResultDict[param.type]++;
            if (param.success && param.index == comboIndex + 1)
            {
                comboIndex = param.index;
                comboCount++;
            }
            else
            {
                comboIndex = param.index;
                comboCount = 0;
            }
            scaleSum += param.scale;
            scaleAmount++;

            //触发突发事件
            if (!isTriggerEmergency && haveEmergency && param.index >= emergencyStartIndex)
            {
                comboIndex = param.index;
                comboCount = 0;
                CookController.instance.cookGame.component.Pause();
                isTriggerEmergency = true;
                component.PlayEmergency(emergencyConfig);
            }
        }

        private void OnContinueBtnClicked()
        {
            EmergencyFinish();
            isContinue = true;
            CookController.instance.cookGame.component.Continue();
        }

        private void OnStopBtnClicked()
        {
            EmergencyFinish();
            isContinue = false;
            ShowResultUI();
        }

        private void OnGetCookGameResult(bool result)
        {
            ShowResultUI();
        }

        private void EmergencyFinish()
        {
            component.EmergencyFinish();
            GameModel.instance.SpendTime(emergencyConfig.costTime);
            PlayerModel.instance.ApplyEffect(emergencyConfig);
            foreach (var item in emergencyConfig.addItemList)
            {
                RefrigeratorModel.instance.AddNewOutSideItem(item.itemName, item.amount);
            }
        }

        private void ShowResultUI()
        {
            float valueScale = scaleSum / scaleAmount;
            float emergencyStartPercent = (float)emergencyStartIndex / CookController.instance.cookGame.component.clickCount;
            if (haveEmergency)
            {
                if (isContinue)
                {
                    valueScale *= emergencyStartPercent + (1 - emergencyStartPercent) * continueScale;
                }
                else
                {
                    valueScale *= emergencyStartPercent;
                }
            }
            param.completePercent = isContinue ? 1.0f : emergencyStartPercent;
            param.scaleValue = valueScale;
            component.ShowResult(maxCombo,
            CookController.instance.cookGame.component.clickCount,
            beatResultDict[EventParam.BeatResultType.Perfect],
            beatResultDict[EventParam.BeatResultType.Good],
            beatResultDict[EventParam.BeatResultType.Bad],
            beatResultDict[EventParam.BeatResultType.Miss],
            valueScale);
        }

        private EventItemConfig TryCreateEmergency()
        {
            //不能被打断就返回
            if (!config.CanBeInterruptedByType(eEventType.Emergency)) return null;

            if (Random.value < config.emergencyProbability)
            {
                return ConfigManager.instance.eventConfig.GetRandomEmergency(config);
            }
            return null;
        }
    }
}