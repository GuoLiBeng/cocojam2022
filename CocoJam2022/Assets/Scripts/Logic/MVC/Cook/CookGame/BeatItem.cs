using UnityEngine;
using System.Collections.Generic;
using ObjectPool;
using System.Collections;

namespace Logic
{
    public class BeatItem : MonoBehaviour
    {
        public LineRenderer line;
        public SpriteRenderer click;
        public Color unClickColor;
        public Color missColor;
        public Color badColor;
        public Color goodColor;
        public Color perfectColor;

        private float m_radius = 1;
        public float radius
        {
            get => m_radius;
            set
            {
                m_radius = value;
                SetLine(value);
            }
        }
        public int pointCount = 10;
        private List<Vector3> linePoints = new List<Vector3>();

        public float minRadius = 3f;
        public float targetRadius = 1f;
        public float perfectPercent = 0.05f;
        public float goodPercent = 0.1f;
        public float badPercent = 0.3f;

        public float fadeInTime = 0.5f;
        public float allTime = 5f;
        public float FadeOutTime = 0.5f;

        private bool isFinish = false;
        private int index;


        void Awake()
        {
            var eventTriggerListener = EventTriggerListener.Get(click.gameObject);
            eventTriggerListener.onClick += OnClicked;

        }
        public void Init(int index, Vector2 pos)
        {
            gameObject.SetActiveOptimize(true);
            this.index = index;
            transform.position = new Vector3(pos.x, pos.y, transform.position.z);
            click.transform.localScale = new Vector3(targetRadius * 2, targetRadius * 2, 1);
            isFinish = false;
            radius = minRadius;
            SetLineColor(unClickColor);
            StopAllCoroutines();
            StartCoroutine(I_Start());
            StartCoroutine(I_FadeIn());
        }

        IEnumerator I_Start()
        {

            float time = 0;
            while (radius > 0)
            {
                time += Time.deltaTime;
                float radiusPercent = time / allTime;
                radius = Mathf.Max(minRadius * (1 - radiusPercent), 0);
                yield return null;
            }
            EventManager.Get<EventDefine.BeatItemResult>().Fire(new EventParam.BeatItemParam()
            {
                index = this.index,
                success = false,
                scale = 0f,
                type = EventParam.BeatResultType.Miss,
            });

            yield return I_FadeOut();
        }
        IEnumerator I_FadeOut()
        {
            isFinish = true;
            float time = 0;
            while (time < FadeOutTime)
            {
                time += Time.deltaTime;
                float fadeOutPercent = 1 - time / FadeOutTime;
                SetAllAlpha(fadeOutPercent);
                yield return null;
            }
            Destroy();
        }

        IEnumerator I_FadeIn()
        {
            float time = 0;
            while (time < fadeInTime)
            {
                time += Time.deltaTime;
                float fadeInPercent = time / fadeInTime;
                SetAllAlpha(fadeInPercent);
                yield return null;
            }
        }

        private void SetLine(float radius)
        {
            linePoints.Clear();
            for (int i = 0; i < pointCount; i++)
            {
                float angle = 360 * ((float)i / pointCount);
                var point = new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad)) * radius;
                linePoints.Add(point);
            }
            linePoints.Add(linePoints[0]);
            line.positionCount = pointCount + 1;
            line.SetPositions(linePoints.ToArray());
        }

        private void SetLineColor(Color color)
        {
            line.startColor = new Color(color.r, color.g, color.b, line.startColor.a);
            line.endColor = new Color(color.r, color.g, color.b, line.startColor.a);
        }

        private void SetAllAlpha(float alpha)
        {
            line.startColor = new Color(line.startColor.r, line.startColor.g, line.startColor.b, alpha);
            line.endColor = new Color(line.endColor.r, line.endColor.g, line.endColor.b, alpha);
            click.color = new Color(click.color.r, click.color.g, click.color.b, alpha);
        }

        private void OnClicked(GameObject obj)
        {
            if (isFinish) return;
            EventParam.BeatItemParam param = new EventParam.BeatItemParam();
            param.index = this.index;
            float completePercent = Mathf.Abs(radius - targetRadius) / (minRadius - targetRadius);
            if (completePercent < perfectPercent)
            {
                SetLineColor(perfectColor);
                param.success = true;
                param.scale = 2.0f;
                param.type = EventParam.BeatResultType.Perfect;
            }
            else if (completePercent < goodPercent)
            {
                SetLineColor(goodColor);
                param.success = true;
                param.scale = 1.0f;
                param.type = EventParam.BeatResultType.Good;
            }
            else if (completePercent < badPercent)
            {
                SetLineColor(badColor);
                param.success = true;
                param.scale = 0.8f;
                param.type = EventParam.BeatResultType.Bad;
            }
            else
            {
                SetLineColor(missColor);
                param.success = false;
                param.scale = 0f;
                param.type = EventParam.BeatResultType.Miss;
            }
            EventManager.Get<EventDefine.BeatItemResult>().Fire(param);
            StopAllCoroutines();
            StartCoroutine(I_FadeOut());
        }

        public void Destroy()
        {
            StopAllCoroutines();
            ObjectPoolManager.instance.ObjectPool.Push("BeatItem", gameObject);
        }
    }
}