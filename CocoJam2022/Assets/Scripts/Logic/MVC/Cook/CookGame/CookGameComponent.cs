using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ObjectPool;

namespace Logic
{
    public class CookGameComponent : MonoBehaviour
    {
        public Vector2[] fixedPos;
        public int clickCount = 20;
        public float intervalMin = 0.5f;
        public float intervalMax = 2.0f;
        public Vector2 rangeMin;
        public Vector2 rangeMax;
        private int currentIndex;
        private Vector2 lastRandomPos = Vector2.zero;
        public void Init()
        {
            currentIndex = 0;
            StopAllCoroutines();
            StartCoroutine(I_Start());
        }

        public void Destroy()
        {
            var beatItems = transform.GetComponentsInChildren<BeatItem>();
            foreach (var item in beatItems)
            {
                item.Destroy();
            }
            StopAllCoroutines();
        }

        public void Pause()
        {
            currentIndex++;
            StopAllCoroutines();
        }

        public void Continue()
        {
            StopAllCoroutines();
            StartCoroutine(I_Start());
        }

        IEnumerator I_Start()
        {
            for (int i = currentIndex; i < clickCount; i++)
            {
                currentIndex = i;
                CreateBeatItem(i);
                yield return new WaitForSeconds(Random.Range(intervalMin, intervalMax));
            }
            yield return new WaitForSeconds(1.0f);
            EventManager.Get<EventDefine.CookGameResult>().Fire(true);
        }

        private void CreateBeatItem(int index)
        {
            Vector2 pos = Vector2.zero;
            if (index < fixedPos.Length)
            {
                pos = fixedPos[index];
            }
            else
            {
                while (true)
                {
                    pos = new Vector2(Random.Range(rangeMin.x, rangeMax.x), Random.Range(rangeMin.y, rangeMax.y));
                    if (Vector2.Distance(pos, lastRandomPos) > 3)
                    {
                        lastRandomPos = pos;
                        break;
                    }
                }
            }

            var beatItem = ObjectPoolManager.instance.ObjectPool.Pop("BeatItem").GetComponent<BeatItem>();
            beatItem.transform.parent = transform;
            beatItem.Init(index, pos);
        }
    }
}