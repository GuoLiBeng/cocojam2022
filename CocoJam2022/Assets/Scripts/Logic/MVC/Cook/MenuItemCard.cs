using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class MenuItemCard : MonoBehaviour
    {
        public Text nameTxt;
        public MultiItemManager matList;
        public Text noneTxt;
        public Button cookBtn;
        public Text costTimeTxt;
        public MenuConfig menuConfig;

        void Awake()
        {
            cookBtn.onClick.AddListener(OnCookBtnClick);
        }

        public void SetData(MenuConfig config)
        {
            menuConfig = config;
            nameTxt.text = config.name;
            costTimeTxt.text = $"耗时:{StaticUtils.MinuteToStringChinese(config.costTime)}";
            bool canCook = true;
            if (config.materials.Length > 0)
            {
                matList.gameObject.SetActiveOptimize(true);
                noneTxt.gameObject.SetActiveOptimize(false);
                matList.SetSize(config.materials.Length);
                for (int i = 0; i < config.materials.Length; i++)
                {
                    var matCard = matList.GetItem<MatCard>(i);
                    matCard.SetData(config.materials[i]);
                    if (!matCard.enough) canCook = false;
                }
            }
            else
            {
                matList.gameObject.SetActiveOptimize(false);
                noneTxt.gameObject.SetActiveOptimize(true);
            }

            cookBtn.gameObject.SetActiveOptimize(canCook);
        }

        private void OnCookBtnClick()
        {
            CookController.instance.HideCookUI();
            GameController.instance.ChangeState<MenuConfig>(eGameOperatorState.Cook, menuConfig);
        }
    }
}