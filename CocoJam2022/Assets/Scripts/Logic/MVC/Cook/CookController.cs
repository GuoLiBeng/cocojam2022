using Config;

namespace Logic
{
    public class CookController : SingletonClass<CookController>, IController<CookModel>
    {
        public CookModel model => CookModel.instance;
        public CookUI cookUI;
        public CookGame cookGame;
        public CookGameUI cookGameUI;
        public void Init()
        {
            model.Init();
            InitViews();
        }

        private void InitViews()
        {
            cookUI = new CookUI();
            cookUI.Init();
        }
        public void Destroy()
        {
            model.Destroy();
            DestroyViews();
        }

        private void DestroyViews()
        {
            cookUI.Destroy();
            cookUI = null;
        }

        public void ShowCookUI()
        {
            cookUI.Show();
        }

        public void HideCookUI()
        {
            cookUI.Hide();
        }

        public void OpenCookGame()
        {
            if (cookGame == null)
            {
                cookGame = new CookGame();
                cookGame.Init();
            }
            cookGame.Show();
        }

        public void HideCookGame()
        {
            if (cookGame != null)
            {
                cookGame.Hide();
            }
        }

        public void ShowCookGameUI()
        {
            if (cookGameUI == null)
            {
                cookGameUI = new CookGameUI();
                cookGameUI.Init();
            }
            cookGameUI.Show();
        }

        public void HideCookGameUI()
        {
            cookGameUI.Hide();
        }
    }
}