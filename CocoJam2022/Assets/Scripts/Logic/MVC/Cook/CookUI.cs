using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class CookUI : BaseUI
    {
        protected override string prefabName => "CookUI";

        protected override UISort sort => UISort.CookUI;

        private Text text;
        private EventItemConfig config => ConfigManager.instance.eventConfig.GetConfigByType(eEventType.Cook);
        private CookComponent component;
        private MenuConfig[] menuConfigs => ConfigManager.instance.cookConfig.menuConfigs;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "exit":
                    Hide();
                    break;
            }
        }

        protected override void OnInit()
        {
            component = sceneObject.GetComponent<CookComponent>();
        }

        protected override void OnShow()
        {
            component.menuList.SetSize(menuConfigs.Length);
            for (int i = 0; i < menuConfigs.Length; i++)
            {
                var menuItem = component.menuList.GetItem<MenuItemCard>(i);
                menuItem.SetData(menuConfigs[i]);
            }
        }
        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }
    }
}