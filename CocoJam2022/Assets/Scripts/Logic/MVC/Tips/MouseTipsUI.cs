using UnityEngine;
using Config;
using Cameras;
using UnityEngine.UI;

namespace Logic
{
    public class MouseTipsUI : BaseMouseMoveUI
    {
        protected override UISort sort => UISort.MouseTipsUI;

        protected override string prefabName => "MouseTipsUI";
        protected override float offsetY => 20;
        protected override float offsetX => 20;
        protected override RectTransform bodyTransform => tipsTxt.transform.parent as RectTransform;

        private Text tipsTxt;

        public void Show(string tips)
        {
            Show();
            tipsTxt.text = tips;
        }

        protected override void OnInit()
        {
            tipsTxt = GetChild<Text>("bg/tips");
        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}