using System.Collections.Generic;
using Config;
using UnityEngine;
using System;

namespace Logic
{
    public class TipsController : SingletonClass<TipsController>, IController<TipsModel>
    {
        public TipsModel model => TipsModel.instance;

        public EventInfoUI eventInfoUI;
        public MouseTipsUI mouseTipsUI;
        public ItemInfoUI itemInfoUI;
        public TipsUI tipsUI;
        public SelectUI selectUI;
        public SelectUI2 selectUI2;
        public void Init()
        {
            model.Init();
            InitViews();
        }

        public void InitViews()
        {
            eventInfoUI = new EventInfoUI();
            eventInfoUI.Init();
            mouseTipsUI = new MouseTipsUI();
            mouseTipsUI.Init();
            itemInfoUI = new ItemInfoUI();
            itemInfoUI.Init();
            tipsUI = new TipsUI();
            tipsUI.Init();
        }
        public void Destroy()
        {
            DestroyViews();
            model.Destroy();
        }

        public void DestroyViews()
        {
            eventInfoUI.Destroy();
            eventInfoUI = null;
            mouseTipsUI.Destroy();
            mouseTipsUI = null;
            itemInfoUI.Destroy();
            itemInfoUI = null;
            tipsUI.Destroy();
            tipsUI = null;
        }

        public void ShowEventInfo(EventItemConfig config)
        {
            eventInfoUI.Show(config);
        }

        public void HideEventInfo()
        {
            if (eventInfoUI != null)
                eventInfoUI.Hide();
        }

        public void ShowMouseTips(string tips)
        {
            mouseTipsUI.Show(tips);
        }

        public void HideMouseTips()
        {
            if (mouseTipsUI != null)
                mouseTipsUI.Hide();
        }

        public void FixedUpdate()
        {
            if (eventInfoUI.isShow) eventInfoUI.Update();
            if (mouseTipsUI.isShow) mouseTipsUI.Update();
            if (itemInfoUI.isShow) itemInfoUI.Update();
        }

        public void ShowItemInfo(ItemData data)
        {
            itemInfoUI.Show(data);
        }

        public void HideItemInfo()
        {
            if (itemInfoUI != null)
                itemInfoUI.Hide();
        }

        public void ShowTips(string tip)
        {
            tipsUI.Show(tip);
        }

        public void OpenSelectUI(int buttonCount, Action<int, GameObject> initFunc = null, Action<int, GameObject> clickFunc = null)
        {
            if (selectUI == null)
            {
                selectUI = new SelectUI();
                selectUI.Init();
            }
            selectUI.Show(buttonCount, initFunc, clickFunc);
        }

        public void HideSelectUI()
        {
            if (selectUI != null)
            {
                selectUI.Hide();
            }
        }

        public void OpenSelectUI2(string tip, int buttonCount, Action<int, GameObject> initFunc = null, Action<int, GameObject> clickFunc = null, bool canExit = false)
        {
            if (selectUI2 == null)
            {
                selectUI2 = new SelectUI2();
                selectUI2.Init();
            }
            selectUI2.Show(tip, buttonCount, initFunc, clickFunc, canExit);
        }

        public void HideSelectUI2()
        {
            if (selectUI2 != null)
            {
                selectUI2.Hide();
            }
        }
    }
}