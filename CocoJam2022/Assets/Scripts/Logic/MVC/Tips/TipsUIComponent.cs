using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class TipsUIComponent : MonoBehaviour
    {
        private TipsUI m_ui;
        private const float showTime = 2.0f;
        private const float fadeOutTime = 1.0f;
        public Text tips;
        private List<string> tipsList = new List<string>();
        private bool isShowing = false;
        void OnDisable()
        {
            StopAllCoroutines();
        }

        public void Init(TipsUI ui)
        {
            m_ui = ui;
        }

        public void Show(string tips)
        {
            tipsList.Add(tips);
            if (!isShowing)
            {
                StopAllCoroutines();
                StartCoroutine(I_Start());
            }
        }

        IEnumerator I_Start()
        {
            while (tipsList.Count > 0)
            {
                yield return I_ShowTips(tipsList[0], tipsList.Count == 1 ? 2.0f : 1.0f);
                tipsList.RemoveAt(0);
            }
            yield return I_FadeOut();
            m_ui.Hide();
        }

        IEnumerator I_ShowTips(string tips, float time)
        {
            isShowing = true;
            GetComponent<CanvasGroup>().alpha = 1;
            this.tips.text = tips;
            yield return new WaitForSeconds(time);
            isShowing = false;
        }

        IEnumerator I_FadeOut()
        {
            float time = 0;
            while (time < 1)
            {
                time += Time.deltaTime;
                GetComponent<CanvasGroup>().alpha = 1 - time / fadeOutTime;
                yield return null;
            }
        }

    }
}