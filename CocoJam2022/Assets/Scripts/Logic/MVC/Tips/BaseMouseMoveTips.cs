using UnityEngine;
using Config;
using Cameras;
using UnityEngine.UI;

namespace Logic
{
    public abstract class BaseMouseMoveUI : BaseUI
    {
        protected virtual float offsetY { get; }
        protected virtual float offsetX { get; }

        protected void SetWorldPos(Vector3 worldPos)
        {
            Vector2 showPos = Vector2.zero;
            var screenPos = SceneCamera.instance.cameraObject.WorldToScreenPoint(worldPos);  //左下角为(0, 0)
            SetScreenPos(screenPos);
        }

        protected virtual RectTransform bodyTransform => rectTransform;

        protected void SetScreenPos(Vector2 screenPos)
        {
            Vector2 showPos = new Vector2(screenPos.x + offsetX, screenPos.y + offsetY);
            if (showPos.y + bodyTransform.sizeDelta.y > Screen.height)
            {
                showPos.y = screenPos.y - bodyTransform.sizeDelta.y - offsetY;
            }

            if (showPos.x + bodyTransform.sizeDelta.x > Screen.width)
            {
                showPos.x = screenPos.x - bodyTransform.sizeDelta.x - offsetX;
            }

            bodyTransform.anchoredPosition = showPos;
        }
        public virtual void Update()
        {
            if (isShow)
            {
                SetScreenPos(Input.mousePosition);
            }
        }
    }
}