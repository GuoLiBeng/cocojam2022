using UnityEngine;
using Config;
using Cameras;
using UnityEngine.UI;

namespace Logic
{
    public class EventInfoUI : BaseMouseMoveUI
    {
        protected override UISort sort => UISort.EventInfoUI;

        protected override string prefabName => "EventInfoUI";
        protected override float offsetY => 20;
        protected override float offsetX => 20;

        private Text name;
        private Text costTime;

        public void Show(EventItemConfig config)
        {
            Show();
            Refresh(config);
        }

        private void Refresh(EventItemConfig config)
        {
            name.text = config.eventName;
            costTime.text = $"消耗时间:{StaticUtils.MinuteToStringChinese(config.costTime)}";
        }

        protected override void OnInit()
        {
            name = GetChild<Text>("layout/name");
            costTime = GetChild<Text>("layout/costTime");
        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}