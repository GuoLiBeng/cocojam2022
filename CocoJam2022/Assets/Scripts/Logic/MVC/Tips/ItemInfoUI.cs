using UnityEngine;
using Config;
using Cameras;
using UnityEngine.UI;

namespace Logic
{
    public class ItemInfoUI : BaseMouseMoveUI
    {
        protected override UISort sort => UISort.ItemInfoUI;

        protected override string prefabName => "ItemInfoUI";
        protected override float offsetY => 20;
        protected override float offsetX => 20;

        private Text nameTxt;
        private Text lifeTxt;
        private Text descTxt;
        private GameObject canEatTxt;

        public void Show(ItemData data)
        {
            Show();
            nameTxt.text = data.config.itemName;
            lifeTxt.text = $"保质期还有{StaticUtils.MinuteToStringChinese(data.life)}";
            descTxt.text = data.config.desc;
            canEatTxt.SetActiveOptimize(data.config.canDirectlyEat);
        }

        protected override void OnInit()
        {
            nameTxt = GetChild<Text>("layout/name");
            lifeTxt = GetChild<Text>("layout/life");
            descTxt = GetChild<Text>("layout/desc");
            canEatTxt = GetChild("layout/canEat");
        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}