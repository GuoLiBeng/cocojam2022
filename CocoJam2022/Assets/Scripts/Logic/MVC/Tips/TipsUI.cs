using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

namespace Logic
{
    public class TipsUI : BaseUI
    {
        protected override UISort sort => UISort.TipsUI;

        protected override string prefabName => "TipsUI";

        private TipsUIComponent component;


        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            component = sceneObject.GetComponent<TipsUIComponent>();
            component.Init(this);
        }

        protected override void OnShow()
        {

        }

        public void Show(string tips)
        {
            base.Show();
            component.Show(tips);
        }
    }
}