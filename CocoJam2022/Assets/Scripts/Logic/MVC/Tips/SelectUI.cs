using UnityEngine;
using Config;
using System;

namespace Logic
{
    public class SelectUI : BaseUI
    {
        protected override UISort sort => UISort.SelectUI;

        protected override string prefabName => "SelectUI";
        private MultiItemManager buttons;
        private Action<int, GameObject> clickCallback;

        protected override void OnInit()
        {
            buttons = GetChild<MultiItemManager>("buttons");
        }

        public void Show(int buttonCount, Action<int, GameObject> initFunc = null, Action<int, GameObject> clickFunc = null)
        {
            buttons.SetSize(buttonCount);
            clickCallback = clickFunc;
            if (initFunc != null)
            {
                for (int i = 0; i < buttonCount; i++)
                {
                    initFunc(i, buttons.GetItem(i));
                }
            }
            base.Show();
        }

        protected override void OnShow()
        {
        }

        protected override void OnHide()
        {
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "exit")
            {
                Hide();
                return;
            }
            if (clickCallback != null)
            {
                var index = buttons.GetItemIndex(obj);
                if (index >= 0)
                {
                    AudioManager.instance.PlayEffect("UI_click");
                    clickCallback(index, obj);
                }
            }
        }

    }
}