using UnityEngine;
using Config;
using System;
using UnityEngine.UI;

namespace Logic
{
    public class SelectUI2 : BaseUI
    {
        protected override UISort sort => UISort.SelectUI2;

        protected override string prefabName => "SelectUI2";
        private MultiItemManager buttons;
        private Action<int, GameObject> clickCallback;
        private Text tipTxt;
        private bool canExit = false;

        protected override void OnInit()
        {
            buttons = GetChild<MultiItemManager>("bg/buttons");
            tipTxt = GetChild<Text>("bg/tip");
        }

        public void Show(string tip, int buttonCount, Action<int, GameObject> initFunc = null, Action<int, GameObject> clickFunc = null, bool canExit = false)
        {
            tipTxt.text = tip;
            canExit = false;
            buttons.SetSize(buttonCount);
            ResetAllClicks();
            clickCallback = clickFunc;
            if (initFunc != null)
            {
                for (int i = 0; i < buttonCount; i++)
                {
                    initFunc(i, buttons.GetItem(i));
                }
            }
            base.Show();
        }

        protected override void OnShow()
        {
        }

        protected override void OnHide()
        {
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "exit")
            {
                if (canExit)
                {
                    Hide();
                    return;
                }
            }
            if (clickCallback != null)
            {
                var index = buttons.GetItemIndex(obj);
                if (index >= 0)
                {
                    AudioManager.instance.PlayEffect("UI_click");
                    clickCallback(index, obj);
                }
            }
        }
    }
}