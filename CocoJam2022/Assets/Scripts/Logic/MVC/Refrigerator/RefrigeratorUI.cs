using UnityEngine;
using UnityEngine.UI;
using Config;
using System.Collections.Generic;
using System.Collections;

namespace Logic
{
    public class RefrigeratorUI : BaseUI
    {
        // private Button btnLeft;
        // private Button btnRight;
        private RectTransform upTrans;
        private RectTransform downTrans;
        private Transform upMgr;
        private Transform downMgr;

        public Transform OutSideRoot;
        public Transform UpRoot;
        public Transform DownRoot;
        public Transform MovingRoot;

        private RefrigeratorController ctrl => RefrigeratorController.instance;
        protected override string prefabName => "RefrigeratorUI";

        protected override UISort sort => UISort.RefrigeratorUI;
        private RefrigeratorType m_type = RefrigeratorType.Up;
        public RefrigeratorType type
        {
            get => m_type;
            set
            {
                m_type = value;
                GetChild<RectTransform>("right").anchoredPosition = m_type == RefrigeratorType.Up ? new Vector2(467, -424) : new Vector2(467, 406);
            }
        }
        private int currentPage = 0;

        private int currentMaxPage => 0;
        private Vector2Int m_upSize = ConfigManager.instance.refrigeratorConfig.upSize;
        private Vector2Int m_downSize = ConfigManager.instance.refrigeratorConfig.downSize;
        public Vector2Int currentSize => type == RefrigeratorType.Up ? m_upSize : m_downSize;
        private Vector2 m_upItemSize = Vector2.zero;
        private Vector2 m_downItemSize = Vector2.zero;
        public Vector2 currentItemSize => type == RefrigeratorType.Up ? m_upItemSize : m_downItemSize;
        private Dictionary<Vector2Int, Vector2> upGridPosToWorldPos;
        private Dictionary<Vector2, Vector2Int> upWorldPosToGridPos;
        private Dictionary<Vector2Int, Vector2> downGridPosToWorldPos;
        private Dictionary<Vector2, Vector2Int> downWorldPosToGridPos;
        private Dictionary<Vector2Int, Vector2> upGridPosToUIPos;
        private Dictionary<Vector2Int, Vector2> downGridPosToUIPos;
        private Dictionary<Vector2Int, Transform> upItems;
        private Dictionary<Vector2Int, Transform> downItems;



        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnChange":
                    if (type == RefrigeratorType.Up)
                    {
                        type = RefrigeratorType.Down;
                    }
                    else if (type == RefrigeratorType.Down)
                    {
                        type = RefrigeratorType.Up;
                    }
                    RefreshHint();
                    AudioManager.instance.PlayEffect("UI_switch");
                    break;
                case "btnClose":
                    EventManager.Get<EventDefine.SettleResFinished>().Fire();
                    break;
            }

        }
        protected override void OnInit()
        {
            BindComponent();
            InitRefrigerator();
        }

        private void BindComponent()
        {
            upTrans = GetChild<RectTransform>("right/up");
            downTrans = GetChild<RectTransform>("right/down");
            upMgr = GetChild<Transform>("right/up/Content");
            downMgr = GetChild<Transform>("right/down/Content");
            OutSideRoot = GetChild("left").transform;
            UpRoot = GetChild("right/up").transform;
            DownRoot = GetChild("right/down").transform;
            MovingRoot = GetChild("moving").transform;
        }

        private void InitRefrigerator()
        {
            type = RefrigeratorType.Up;
            m_upItemSize = (upMgr.GetChild(0) as RectTransform).sizeDelta;
            upGridPosToWorldPos = new Dictionary<Vector2Int, Vector2>();
            upWorldPosToGridPos = new Dictionary<Vector2, Vector2Int>();
            upGridPosToUIPos = new Dictionary<Vector2Int, Vector2>();
            upItems = new Dictionary<Vector2Int, Transform>();
            for (int i = 0; i < upMgr.transform.childCount; i++)
            {
                var item = upMgr.transform.GetChild(i);
                Vector2Int gridPos = GetGridPosByIndex(i, m_upSize);
                upGridPosToWorldPos.Add(gridPos, item.transform.position);
                upWorldPosToGridPos.Add(item.transform.position, gridPos);
                upGridPosToUIPos.Add(gridPos, (item.transform as RectTransform).anchoredPosition);
                upItems.Add(gridPos, item);
            }

            //要先切换到下边模式  才能获取正确的世界坐标
            type = RefrigeratorType.Down;
            m_downItemSize = (downMgr.GetChild(0) as RectTransform).sizeDelta;
            downGridPosToWorldPos = new Dictionary<Vector2Int, Vector2>();
            downWorldPosToGridPos = new Dictionary<Vector2, Vector2Int>();
            downGridPosToUIPos = new Dictionary<Vector2Int, Vector2>();
            downItems = new Dictionary<Vector2Int, Transform>();
            for (int i = 0; i < downMgr.transform.childCount; i++)
            {
                var item = downMgr.transform.GetChild(i);
                Vector2Int gridPos = GetGridPosByIndex(i, m_downSize);
                downGridPosToWorldPos.Add(gridPos, item.transform.position);
                downWorldPosToGridPos.Add(item.transform.position, gridPos);
                downGridPosToUIPos.Add(gridPos, (item.transform as RectTransform).anchoredPosition);
                downItems.Add(gridPos, item);
            }
            type = RefrigeratorType.Up;
        }

        private Vector2Int GetGridPosByIndex(int index, Vector2Int size)
        {
            int x = index / size.y;
            int y = index % size.y;
            return new Vector2Int(x, y);
        }

        protected override void OnShow()
        {
            AudioManager.instance.PlayAGM("冰箱_hum_lp");
            AudioManager.instance.PlayEffect("冰箱门_开启");
            EventManager.Get<EventDefine.RefrigeratorHint>().AddListener(UpdateHint);
            EventManager.Get<EventDefine.RefrigeratorObstacleUpdate>().AddListener(RefreshHint);
            RefreshHint();
        }

        protected override void OnHide()
        {
            AudioManager.instance.StopAGM();
            AudioManager.instance.PlayEffect("冰箱门_关闭");
            EventManager.Get<EventDefine.RefrigeratorHint>().RemoveListener(UpdateHint);
            EventManager.Get<EventDefine.RefrigeratorObstacleUpdate>().RemoveListener(RefreshHint);
            RefrigeratorController.instance.DestroyAllItem();
        }

        protected override void OnDestroy()
        {
            RefrigeratorController.instance.DestroyAllItem();
        }

        protected override void RegisterEvents()
        {

        }

        protected override void UnRegisterEvents()
        {

        }
        private int initRefrigeratorCount = 5;
        public void Update()
        {
            if (!isShow) return;
        }

        public void AddOutSide(RectTransform trans)
        {
            trans.SetParent(OutSideRoot, true);
        }

        public void AddRefrigerator(RectTransform trans, RefrigeratorType type)
        {
            Transform root = null;
            if (type == RefrigeratorType.Up)
            {
                root = UpRoot;
            }
            else if (type == RefrigeratorType.Down)
            {
                root = DownRoot;
            }
            //if (root = null) return;
            trans.SetParent(root, true);
        }

        public void AddMoving(RectTransform trans)
        {
            trans.SetParent(MovingRoot, true);
        }

        public Vector2 GridPosToWorldPos(RefrigeratorType type, Vector2Int gridPos)
        {
            if (type == RefrigeratorType.Up)
            {
                return upGridPosToWorldPos[gridPos];
            }
            else if (type == RefrigeratorType.Down)
            {
                return downGridPosToWorldPos[gridPos];
            }
            return Vector2.zero;
        }

        public Vector2 GridPosToUIPos(RefrigeratorType type, Vector2Int gridPos)
        {
            if (type == RefrigeratorType.Up)
            {
                return upGridPosToUIPos[gridPos];
            }
            else if (type == RefrigeratorType.Down)
            {
                return downGridPosToUIPos[gridPos];
            }
            return Vector2.zero;
        }

        public bool WorldPosToGridPos(RefrigeratorType type, Vector2 worldPos, out Vector2Int gridPos)
        {
            if (type == RefrigeratorType.Up)
            {
                foreach (var pair in upWorldPosToGridPos)
                {
                    var pos = pair.Key;
                    if (Mathf.Abs(pos.x - worldPos.x) < m_upItemSize.x / 2 && Mathf.Abs(pos.y - worldPos.y) < m_upItemSize.y / 2)
                    {
                        gridPos = pair.Value;
                        return true;
                    }
                }
            }
            else if (type == RefrigeratorType.Down)
            {
                foreach (var pair in downWorldPosToGridPos)
                {
                    var pos = pair.Key;
                    if (Mathf.Abs(pos.x - worldPos.x) < m_downItemSize.x / 2 && Mathf.Abs(pos.y - worldPos.y) < m_downItemSize.y / 2)
                    {
                        gridPos = pair.Value;
                        return true;
                    }
                }
            }
            gridPos = Vector2Int.zero;
            return false;
        }

        private EventParam.RefrigeratorHint hintData;
        private void UpdateHint(EventParam.RefrigeratorHint hint)
        {
            hintData = hint;
            RefreshHint();
        }

        private void RefreshHint()
        {
            var gridData = ctrl.model.GetGridDataByType(type);
            var items = type == RefrigeratorType.Up ? upItems : downItems;
            foreach (var data in gridData.Values)
            {
                Color color = Color.white;
                if (data.isTakePlace) color = Color.yellow;
                if (data.isObstacle) color = Color.grey;
                items[data.pos].Find("color").GetComponent<Image>().color = color;
            }

            if (hintData != null)
            {
                foreach (var pos in hintData.gridPos)
                {
                    if (items.ContainsKey(pos))
                    {
                        items[pos].Find("color").GetComponent<Image>().color = hintData.color;
                    }
                }
            }
        }
    }
}