using Config;
using UnityEngine;
using System.Collections.Generic;

namespace Logic
{
    public class RefrigeratorController : SingletonClass<RefrigeratorController>, IController<RefrigeratorModel>
    {
        public RefrigeratorModel model => RefrigeratorModel.instance;
        public RefrigeratorUI refrigeratorUI;
        public List<ItemUI> itemList = new List<ItemUI>();
        private List<ItemUI> waitToDestroyList = new List<ItemUI>();
        public void Init()
        {
            model.Init();
            InitViews();
        }

        private void InitViews()
        {
            refrigeratorUI = new RefrigeratorUI();
            refrigeratorUI.Init();
        }
        public void ShowUI()
        {
            refrigeratorUI.Show();
            foreach (var data in model.GetOutSideItems())
            {
                CreateItem(data);
            }
            foreach (var data in model.GetRefrigeratorItems(RefrigeratorType.Up))
            {
                CreateItem(data);
            }
            foreach (var data in model.GetRefrigeratorItems(RefrigeratorType.Down))
            {
                CreateItem(data);
            }
        }

        public void HideUI()
        {
            refrigeratorUI.Hide();
        }

        public void Destroy()
        {
            model.Destroy();
            DestoryViews();
        }

        private void DestoryViews()
        {
            refrigeratorUI.Destroy();
            refrigeratorUI = null;
            DestroyAllItem();
        }

        public void Update()
        {
            refrigeratorUI.Update();
            foreach (var item in itemList)
            {
                item.Update();
            }

            if (waitToDestroyList.Count > 0)
            {
                foreach (var item in waitToDestroyList)
                {
                    DestroyItemImmediately(item);
                }
                waitToDestroyList.Clear();
            }
        }

        public void FixedUpdate()
        {
            foreach (var item in itemList)
            {
                item.FixedUpdate();
            }
        }

        public ItemUI CreateItem(ItemData data)
        {
            var newItem = new ItemUI();
            newItem.Init();
            newItem.Show(data);
            itemList.Add(newItem);
            return newItem;
        }

        public void DestroyItemImmediately(ItemUI item)
        {
            itemList.Remove(item);
            item.Destroy();
        }

        public void DestroyItem(ItemUI item)
        {
            waitToDestroyList.Add(item);
        }

        public void DestroyAllItem()
        {
            foreach (var item in itemList)
            {
                item.Destroy();
            }
            itemList.Clear();
        }

        public bool CheckCanPlace(ItemData data)
        {
            return model.CheckCanPlace(data, refrigeratorUI.type);
        }
    }
}