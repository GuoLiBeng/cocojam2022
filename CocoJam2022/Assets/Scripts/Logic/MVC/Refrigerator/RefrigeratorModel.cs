using System.Collections.Generic;
using UnityEngine;
using Config;
using System.Text;

namespace Logic
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
    }

    public enum RefrigeratorType
    {
        Up,
        Down,
    }
    //可存档数据
    public class ItemData
    {
        public ItemConfigItem config;
        public bool isOutSide;
        private Vector2Int m_pos;
        public Vector2Int pos
        {
            get => m_pos;
            set
            {
                m_pos = value;
                m_takeOverGrid = null;
            }
        }
        private Direction m_dir = Direction.Up;
        public Direction dir
        {
            get => m_dir;
            set
            {
                m_dir = value;
                m_takeOverGrid = null;
            }
        }
        private Vector2Int[] m_takeOverGrid;
        public Vector2Int[] takeOverGrid
        {
            get
            {
                if (m_takeOverGrid == null)
                {
                    Vector2Int[] grids = new Vector2Int[config.shape.Length + 1];

                    for (int i = 0; i < config.shape.Length; i++)
                    {
                        var shapePos = config.shape[i];
                        grids[i] = pos + StaticUtils.RotatePos(shapePos, Vector2Int.zero, dir);
                    }
                    grids[config.shape.Length] = pos;
                    return grids;
                }
                return m_takeOverGrid;
            }
        }
        public int life;
        public bool isOutOfLife => life == 0;
        public RefrigeratorType type;
    }

    //初始化后生成数据
    public class GridData
    {
        public Vector2Int pos;
        public ItemData takePlaceItem;
        public bool isTakePlace => takePlaceItem != null;
        public bool isObstacle;
    }
    public class RefrigeratorModel : SingletonClass<RefrigeratorModel>, IModel
    {
        private const float RefrigeratorSlowScale = 0.5f;
        private List<ItemData> upItems;
        private List<ItemData> downItems;
        private List<ItemData> outSideItems;
        public Dictionary<Vector2Int, GridData> upGridDatas;
        public Dictionary<Vector2Int, GridData> downGridDatas;
        private void InitItems()
        {
            foreach (var item in ConfigManager.instance.refrigeratorConfig.initRes)
            {
                AddNewOutSideItem(item.itemName, item.amount);
            }
        }

        public void Init()
        {
            //可读取存档数据
            upItems = new List<ItemData>();
            downItems = new List<ItemData>();
            outSideItems = new List<ItemData>();

            InitItems();

            //构建网格
            upGridDatas = new Dictionary<Vector2Int, GridData>();
            Vector2Int upSize = ConfigManager.instance.refrigeratorConfig.upSize;
            for (int i = 0; i < upSize.x; i++)
            {
                for (int j = 0; j < upSize.y; j++)
                {
                    var pos = new Vector2Int(i, j);
                    upGridDatas.Add(pos, new GridData()
                    {
                        pos = pos,
                        isObstacle = false,
                    });
                }
            }


            downGridDatas = new Dictionary<Vector2Int, GridData>();
            Vector2Int downSize = ConfigManager.instance.refrigeratorConfig.downSize;
            for (int i = 0; i < downSize.x; i++)
            {
                for (int j = 0; j < downSize.y; j++)
                {
                    var pos = new Vector2Int(i, j);
                    downGridDatas.Add(pos, new GridData()
                    {
                        pos = pos,
                        isObstacle = false,
                    });
                }
            }

            RefreshObstacle();

            //填充数据
            foreach (var item in upItems)
            {
                AddItemDataToGridData(item, RefrigeratorType.Up);
            }

            foreach (var item in downItems)
            {
                AddItemDataToGridData(item, RefrigeratorType.Down);
            }

            EventManager.Get<EventDefine.TimeUpdate>().AddListener(OnTimeUpdate);
            EventManager.Get<EventDefine.AbilityUpgrade>().AddListener(OnAbilityUpgrade);
        }

        private void OnAbilityUpgrade(eAbilityType type, int newLevel)
        {
            if (type == eAbilityType.Settle)
            {
                RefreshObstacle();
            }
        }

        public void RefreshObstacle()
        {
            //更新阻碍
            var level = PlayerController.instance.model.abilityLevel[eAbilityType.Settle];
            foreach (var obstacle in ConfigManager.instance.refrigeratorConfig.upObstacleConfigs)
            {
                if (upGridDatas.ContainsKey(obstacle.obstaclePos))
                {
                    upGridDatas[obstacle.obstaclePos].isObstacle = level < obstacle.hideLevel;
                }
            }
            foreach (var obstacle in ConfigManager.instance.refrigeratorConfig.downObstacleConfigs)
            {
                if (downGridDatas.ContainsKey(obstacle.obstaclePos))
                {
                    downGridDatas[obstacle.obstaclePos].isObstacle = level < obstacle.hideLevel;
                }
            }
            EventManager.Get<EventDefine.RefrigeratorObstacleUpdate>().Fire();
        }

        private void AddItemDataToGridData(ItemData item, RefrigeratorType type)
        {
            var gridData = GetGridDataByType(type);
            foreach (var gridPos in item.takeOverGrid)
            {
                if (!gridData.ContainsKey(gridPos))
                {
                    //Debug.LogError($"{item.config.itemName} 的坐标 {gridPos.ToString()} 超界了  ");
                }
                else if (gridData[gridPos].takePlaceItem != null)
                {
                    //Debug.LogError($"{item.config.itemName} 和 {gridData[gridPos].takePlaceItem.config.itemName} 在坐标 {gridPos.ToString()} 重叠了 ");
                }
                else if (gridData[gridPos].isObstacle)
                {
                    //Debug.LogError($"{item.config.itemName} 在坐标 {gridPos.ToString()} 有障碍物 ");
                }
                else
                {
                    gridData[gridPos].takePlaceItem = item;
                }
            }
        }

        private void RemoveItemDataFromGridData(ItemData item, RefrigeratorType type)
        {
            var gridData = GetGridDataByType(type);
            foreach (var gridPos in item.takeOverGrid)
            {
                if (!gridData.ContainsKey(gridPos))
                {
                    //Debug.LogError($"{item.config.itemName} 的坐标 {gridPos.ToString()} 超界了  ");
                }
                else
                {
                    gridData[gridPos].takePlaceItem = null;
                }
            }
        }

        public Dictionary<Vector2Int, GridData> GetGridDataByType(RefrigeratorType type)
        {
            switch (type)
            {
                case RefrigeratorType.Up:
                    return upGridDatas;
                case RefrigeratorType.Down:
                    return downGridDatas;
            }
            return null;
        }

        public bool CheckCanPlace(ItemData data, RefrigeratorType type)
        {
            var gridData = GetGridDataByType(type);
            foreach (var gridPos in data.takeOverGrid)
            {
                if (!gridData.ContainsKey(gridPos))
                {
                    //Debug.Log($"checkTakePlace:false  {gridPos.ToString()}超出边界");
                    return false;
                }
                if (gridData[gridPos].isTakePlace)
                {
                    //Debug.Log($"checkTakePlace:false   {gridPos.ToString()} 已经被占了");
                    return false;
                }
                if (gridData[gridPos].isObstacle)
                {
                    //Debug.Log($"checkTakePlace:false   {gridPos.ToString()} 有障碍物");
                    return false;
                }
            }
            //Debug.Log($"checkTakePlace:true");
            return true;
        }

        public void AddNewOutSideItem(string name, int amount = 1)
        {
            var config = ConfigManager.instance.itemConfig.GetItemConfigByName(name);
            if (config != null)
            {
                for (int i = 0; i < amount; i++)
                {
                    outSideItems.Add(new ItemData()
                    {
                        config = config,
                        isOutSide = true,
                        life = config.Life,
                    });
                }
            }
        }

        private void RemoveOutSideItem(ItemData itemData)
        {
            if (outSideItems.Contains(itemData))
            {
                outSideItems.Remove(itemData);
                EventManager.Get<EventDefine.RefrigeratorObstacleUpdate>().Fire();
            }
        }

        public List<ItemData> GetOutSideItems()
        {
            return outSideItems;
        }

        public List<ItemData> GetRefrigeratorItems(RefrigeratorType type)
        {
            switch (type)
            {
                case RefrigeratorType.Up:
                    return upItems;
                case RefrigeratorType.Down:
                    return downItems;
            }
            return null;
        }

        public void MoveOutSideItemToRefrigerator(ItemData data, RefrigeratorType type)
        {
            outSideItems.Remove(data);
            data.isOutSide = false;
            data.type = type;
            GetRefrigeratorItems(type).Add(data);
            AddItemDataToGridData(data, type);
        }

        public void MoveRefrigeratorToOutSide(ItemData data, RefrigeratorType type)
        {
            RemoveItemDataFromGridData(data, type);
            GetRefrigeratorItems(type).Remove(data);
            data.isOutSide = true;
            outSideItems.Add(data);
        }

        private void RemoveRefrigeratorItem(ItemData itemData)
        {
            var list = GetRefrigeratorItems(itemData.type);
            if (list.Contains(itemData))
            {
                RemoveItemDataFromGridData(itemData, itemData.type);
                list.Remove(itemData);
                EventManager.Get<EventDefine.RefrigeratorObstacleUpdate>().Fire();
            }
        }

        public void Destroy()
        {
            EventManager.Get<EventDefine.AbilityUpgrade>().RemoveListener(OnAbilityUpgrade);
            EventManager.Get<EventDefine.TimeUpdate>().RemoveListener(OnTimeUpdate);
        }

        private void OnTimeUpdate(EventParam.TimeUpdateParam param)
        {
            SpendTime(param.SpendTime);
        }

        public void SpendTime(int minute)
        {
            Dictionary<string, int> itemNameDict = new Dictionary<string, int>();
            foreach (var item in SpendTimeOnList(outSideItems, minute))
            {
                if (!itemNameDict.ContainsKey(item.config.itemName))
                {
                    itemNameDict.Add(item.config.itemName, 1);
                }
                else
                {
                    itemNameDict[item.config.itemName]++;
                }
                RemoveItem(item);
            }

            int refrigeratorTime = (int)(minute * RefrigeratorSlowScale);
            foreach (var item in SpendTimeOnList(GetRefrigeratorItems(RefrigeratorType.Up), refrigeratorTime))
            {
                if (!itemNameDict.ContainsKey(item.config.itemName))
                {
                    itemNameDict.Add(item.config.itemName, 1);
                }
                else
                {
                    itemNameDict[item.config.itemName]++;
                }
                RemoveItem(item);
            }
            foreach (var item in SpendTimeOnList(GetRefrigeratorItems(RefrigeratorType.Down), refrigeratorTime))
            {
                if (!itemNameDict.ContainsKey(item.config.itemName))
                {
                    itemNameDict.Add(item.config.itemName, 1);
                }
                else
                {
                    itemNameDict[item.config.itemName]++;
                }
                RemoveItem(item);
            }

            if (itemNameDict.Count > 0)
            {
                string log = "";
                int count = 0;
                foreach (var pairs in itemNameDict)
                {
                    count++;
                    log += $"{pairs.Key}×{pairs.Value}";
                    if (count < itemNameDict.Count)
                    {
                        log += "、";
                    }
                }
                log += " 已过期";
                TipsController.instance.ShowTips(log);
            }
        }
        public void RemoveItem(ItemData itemData)
        {
            if (itemData.isOutSide)
            {
                RemoveOutSideItem(itemData);
            }
            else
            {
                RemoveRefrigeratorItem(itemData);
            }
        }

        private List<ItemData> SpendTimeOnList(List<ItemData> list, int time)
        {
            List<ItemData> outOfDateItems = new List<ItemData>();
            foreach (var item in list)
            {
                if (item.life > 0)
                {
                    item.life -= time;
                    if (item.life < 0)
                    {
                        item.life = 0;
                    }
                    if (item.life <= 0)
                    {
                        outOfDateItems.Add(item);
                    }
                }
            }
            return outOfDateItems;
        }

        public int GetItemAmount(string name)
        {
            int amount = 0;
            foreach (var item in outSideItems)
            {
                if (item.config.itemName == name)
                {
                    amount++;
                }
            }
            foreach (var item in GetRefrigeratorItems(RefrigeratorType.Up))
            {
                if (item.config.itemName == name)
                {
                    amount++;
                }
            }
            foreach (var item in GetRefrigeratorItems(RefrigeratorType.Down))
            {
                if (item.config.itemName == name)
                {
                    amount++;
                }
            }
            return amount;
        }

        public bool ConsumeItem(string name, int amount)
        {
            int haveAmount = 0;
            List<ItemData> waitToConsumeItems = new List<ItemData>();
            foreach (var item in outSideItems)
            {
                if (item.config.itemName == name)
                {
                    waitToConsumeItems.Add(item);
                    haveAmount++;
                }
            }
            foreach (var item in GetRefrigeratorItems(RefrigeratorType.Up))
            {
                if (item.config.itemName == name)
                {
                    waitToConsumeItems.Add(item);
                    haveAmount++;
                }
            }
            foreach (var item in GetRefrigeratorItems(RefrigeratorType.Down))
            {
                if (item.config.itemName == name)
                {
                    waitToConsumeItems.Add(item);
                    haveAmount++;
                }
            }
            if (haveAmount >= amount)
            {
                foreach (var item in waitToConsumeItems)
                {
                    RemoveItem(item);
                }
                return true;
            }
            return false;
        }

        public void AddRandomItems()
        {
            Dictionary<string, int> itemDict = new Dictionary<string, int>();
            int typeAmount = Random.Range(ConfigManager.instance.buyConfig.minItemTypeAmount, ConfigManager.instance.buyConfig.maxItemTypeAmount);

            for (int i = 0; i < typeAmount; i++)
            {
                string itemName = ConfigManager.instance.itemConfig.items[Random.Range(0, ConfigManager.instance.itemConfig.items.Length)].itemName;
                int amount = Random.Range(ConfigManager.instance.buyConfig.minItemAmount, ConfigManager.instance.buyConfig.maxItemAmount);
                AddNewOutSideItem(itemName, amount);
                if (itemDict.ContainsKey(itemName))
                {
                    itemDict[itemName] += amount;
                }
                else
                {
                    itemDict.Add(itemName, amount);
                }
            }

            if (itemDict.Count > 0)
            {
                string log = "恭喜抢到了  ";
                int count = 0;
                foreach (var pairs in itemDict)
                {
                    count++;
                    log += $"{pairs.Key}×{pairs.Value}";
                    if (count < itemDict.Count)
                    {
                        log += "、";
                    }
                }
                AudioManager.instance.PlayRewardSound();
                TipsController.instance.ShowTips(log);
            }
        }

        public bool HaveOutSideCoca()
        {
            foreach (var item in outSideItems)
            {
                if (item.config.itemName == "可乐")
                {
                    return true;
                }
            }
            return false;
        }

        public void ConsumeOutSideCoca()
        {
            foreach (var item in outSideItems)
            {
                if (item.config.itemName == "可乐")
                {
                    PlayerModel.instance.ApplyEffect(item.config);
                    RemoveItem(item);
                    break;
                }
            }
        }
    }
}