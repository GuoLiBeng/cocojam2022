using UnityEngine;
using Config;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace Logic
{
    public class ItemUI : BaseUI
    {
        private RefrigeratorController ctrl => RefrigeratorController.instance;
        private Image img;
        protected override UISort sort => UISort.ItemUI;
        protected override string prefabName => "ItemUI";
        private Vector2 outsideOrigin => new Vector2(0, 0);
        private float outsideRadius => 350;
        private Vector2 outsidePos;
        private Vector2 targetPos;
        private Vector2 dragStartPointerPos;
        private bool isDraging = false;
        private Quaternion outsideRotate;
        private Quaternion targetRotate = Quaternion.Euler(0, 0, 0);
        private Direction currentDir
        {
            get { return data.dir; }
            set { data.dir = value; }
        }
        private Quaternion movingRotate
        {
            get
            {
                switch (currentDir)
                {
                    case Direction.Up:
                        return Quaternion.Euler(0, 0, 0);
                    case Direction.Right:
                        return Quaternion.Euler(0, 0, -90);
                    case Direction.Down:
                        return Quaternion.Euler(0, 0, 180);
                    case Direction.Left:
                        return Quaternion.Euler(0, 0, 90);
                }
                return Quaternion.Euler(0, 0, 90);
            }
        }

        private ItemData data;

        private float targetScale;
        private const float outSideScale = 1.0f;
        private const float upScale = 1.0f;
        private const float downScale = 0.67f;

        protected override void OnClick(GameObject obj)
        {

        }
        protected override void OnInit()
        {
            img = GetChild<Image>("icon");
            var eventListener = EventTriggerListener.Get(img.gameObject);
            eventListener.onDown += OnDown;
            eventListener.onUp += OnUp;
            eventListener.onDrag += OnDrag;
            eventListener.onEnter += OnEnter;
            eventListener.onExit += OnExit;
        }
        public void Show(ItemData itemData)
        {
            data = itemData;
            base.Show();
        }

        private Vector2 GetRandomOutSidePos()
        {
            return outsideOrigin + Random.insideUnitCircle * Random.Range(0, outsideRadius);
        }

        private Quaternion GetRandomOutSideRotation()
        {
            return Quaternion.Euler(0, 0, Random.Range(0, 360));
        }

        protected override void OnShow()
        {
            img.sprite = data.config.icon;
            img.SetToSpriteCenter();
            img.rectTransform.localScale = new Vector3(data.config.imgScale, data.config.imgScale, 1);
            outsidePos = GetRandomOutSidePos();
            if (data.isOutSide)
            {
                ctrl.refrigeratorUI.AddOutSide(rectTransform);
                targetPos = outsidePos;
                rectTransform.anchoredPosition = outsidePos;

                targetRotate = GetRandomOutSideRotation();
                rectTransform.rotation = targetRotate;

                targetScale = outSideScale;
                rectTransform.localScale = new Vector3(targetScale, targetScale, 1);
            }
            else
            {
                ctrl.refrigeratorUI.AddRefrigerator(rectTransform, data.type);
                targetPos = ctrl.refrigeratorUI.GridPosToUIPos(data.type, data.pos);
                rectTransform.anchoredPosition = targetPos;

                targetRotate = movingRotate;
                rectTransform.rotation = targetRotate;

                targetScale = data.type == RefrigeratorType.Up ? upScale : downScale;
                rectTransform.localScale = new Vector3(targetScale, targetScale, 1);
            }
        }

        protected override void OnHide()
        {
            data = null;
        }

        protected override void OnDestroy()
        {
            var eventListener = EventTriggerListener.Get(img.gameObject);
            eventListener.onDown -= OnDown;
            eventListener.onUp -= OnUp;
            eventListener.onDrag -= OnDrag;
            eventListener.onEnter -= OnEnter;
            eventListener.onExit -= OnExit;
        }
        public void Update()
        {
            if (data.config.canDirectlyEat && Input.GetKeyDown(KeyCode.E) && isPointerAbove)
            {
                if (data.config.itemName == "可乐" && !data.isOutSide)
                {
                    TipsController.instance.ShowTips("冰镇可乐效果翻倍!");
                    PlayerModel.instance.ApplyEffect(data.config, 2);
                }
                else
                {
                    PlayerModel.instance.ApplyEffect(data.config);
                }
                if (data.config.isLiquid)
                {
                    AudioManager.instance.PlayEffect("UI_DrinkWater");
                }
                else
                {
                    AudioManager.instance.PlayEffect("UI_EatChew");
                }

                RefrigeratorModel.instance.RemoveItem(data);
                OnExit(sceneObject);
                RefrigeratorController.instance.DestroyItem(this);
            }

            if (Vector2.Distance(rectTransform.anchoredPosition, outsideOrigin) < outsideRadius && Input.GetMouseButtonDown(1) && isPointerAbove)
            {
                ChangeDir();
            }
        }

        public void FixedUpdate()
        {
            if (!isDraging)
            {
                MoveToTargetPos();
            }
            RotateToTargetQuaternion();
            ScaleToTargetScale();
        }

        private void ChangeDir()
        {
            switch (currentDir)
            {
                case Direction.Up:
                    currentDir = Direction.Right;
                    break;
                case Direction.Right:
                    currentDir = Direction.Down;
                    break;
                case Direction.Down:
                    currentDir = Direction.Left;
                    break;
                case Direction.Left:
                    currentDir = Direction.Up;
                    break;
            }
            targetRotate = movingRotate;
        }

        private void MoveToTargetPos()
        {
            if (Vector2.Distance(rectTransform.anchoredPosition, targetPos) > 0.02f)
            {
                rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, targetPos, 0.2f);
            }
        }

        private void RotateToTargetQuaternion()
        {
            if (Quaternion.Angle(rectTransform.rotation, targetRotate) > 1)
            {
                rectTransform.rotation = Quaternion.Lerp(rectTransform.rotation, targetRotate, 0.2f);
            }
        }

        private void ScaleToTargetScale()
        {
            if (Mathf.Abs(rectTransform.localScale.x - targetScale) > 0.02f)
            {
                rectTransform.localScale = Vector2.Lerp(rectTransform.localScale, new Vector3(targetScale, targetScale, 1.0f), 0.2f);
            }
        }

        private void OnDown(GameObject obj)
        {
            if (!data.isOutSide && data.type != ctrl.refrigeratorUI.type) return;
            dragStartPointerPos = Input.mousePosition;
            targetRotate = movingRotate;
            targetScale = ctrl.refrigeratorUI.type == RefrigeratorType.Up ? upScale : downScale;
            isDraging = true;
            TipsController.instance.HideItemInfo();
            ctrl.refrigeratorUI.AddMoving(rectTransform);

            if (!data.isOutSide)
            {
                //ctrl.refrigeratorUI.AddOutSide(rectTransform);
                AudioManager.instance.PlayEffect("UI_FoodTakeOut");
                targetPos = outsidePos;
                ctrl.model.MoveRefrigeratorToOutSide(data, ctrl.refrigeratorUI.type);
                dragStartPointerPos -= (rectTransform.anchoredPosition - outsidePos);
            }
            else
            {
                AudioManager.instance.PlayEffect("UI_FoodPickUp");
                AudioManager.instance.PlayEffect(Random.value < 0.5f ? "塑料袋_取物品01" : "塑料袋_取物品02");
            }
        }

        private void OnUp(GameObject obj)
        {
            if (!data.isOutSide && data.type != ctrl.refrigeratorUI.type) return;
            isDraging = false;
            TipsController.instance.ShowItemInfo(data);
            if (data.isOutSide)
            {
                AudioManager.instance.StopSingletonEffect("冰箱_翻找");
                AudioManager.instance.StopSingletonEffect("塑料袋_翻找");
                if (Vector2.Distance(rectTransform.anchoredPosition, outsideOrigin) < outsideRadius)
                {
                    ctrl.refrigeratorUI.AddOutSide(rectTransform);
                    outsidePos = rectTransform.anchoredPosition;
                    targetPos = outsidePos;
                    targetScale = outSideScale;
                    EventManager.Get<EventDefine.RefrigeratorHint>().Fire(null);
                    return;
                }

                if (ctrl.refrigeratorUI.WorldPosToGridPos(ctrl.refrigeratorUI.type, rectTransform.position, out Vector2Int gridPos)
                && RefrigeratorController.instance.CheckCanPlace(data))
                {
                    AudioManager.instance.PlayEffect("UI_FoodPutIn");
                    ctrl.refrigeratorUI.AddRefrigerator(rectTransform, ctrl.refrigeratorUI.type);
                    targetPos = ctrl.refrigeratorUI.GridPosToUIPos(ctrl.refrigeratorUI.type, data.pos);
                    ctrl.model.MoveOutSideItemToRefrigerator(data, ctrl.refrigeratorUI.type);
                    EventManager.Get<EventDefine.RefrigeratorHint>().Fire(null);
                    return;
                }

                ctrl.refrigeratorUI.AddOutSide(rectTransform);
                EventManager.Get<EventDefine.RefrigeratorHint>().Fire(null);
                targetScale = outSideScale;
                targetPos = outsidePos;
            }
        }
        private Vector2Int oldGridPos = Vector2Int.zero;

        private void OnDrag(PointerEventData eventData)
        {
            if (!data.isOutSide && data.type != ctrl.refrigeratorUI.type) return;
            var offset = eventData.position - dragStartPointerPos;
            rectTransform.anchoredPosition = outsidePos + offset;
            if (ctrl.refrigeratorUI.WorldPosToGridPos(ctrl.refrigeratorUI.type, rectTransform.position, out Vector2Int gridPos))
            {
                AudioManager.instance.PlaySingletonEffect("冰箱_翻找");
                data.pos = gridPos;
                if (oldGridPos != gridPos)
                {
                    AudioManager.instance.PlayEffect("UI_MouseOver");
                    oldGridPos = gridPos;
                    EventManager.Get<EventDefine.RefrigeratorHint>().Fire(new EventParam.RefrigeratorHint
                    {
                        gridPos = data.takeOverGrid,
                        color = ctrl.CheckCanPlace(data) ? Color.green : Color.red,
                    });
                }
            }
            else if (Vector2.Distance(rectTransform.anchoredPosition, outsideOrigin) < outsideRadius)
            {
                AudioManager.instance.PlaySingletonEffect("塑料袋_翻找");
            }
            else
            {
                EventManager.Get<EventDefine.RefrigeratorHint>().Fire(null);
            }
        }

        private bool isPointerAbove = false;
        private void OnEnter(GameObject obj)
        {
            isPointerAbove = true;
            TipsController.instance.ShowItemInfo(data);
        }

        private void OnExit(GameObject obj)
        {
            isPointerAbove = false;
            TipsController.instance.HideItemInfo();
        }
    }
}