using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class RefrigeratorItem : MonoBehaviour
    {
        public Image colorImg;

        public void SetData(Color color)
        {
            colorImg.color = color;
        }
    }
}